--insert groups data [MASTER LIST - DO NOT CHANGE]
INSERT INTO groups (name) VALUES
('PSS'),
('QAS');

--insert modules data [MASTER LIST - DO NOT CHANGE]
INSERT INTO modules (name) VALUES
('CRS/CMOR'),
('Shopper'),
('Franchisee'),
('Distribution'),
('Middleware'),
('CWS'),
('Loyalty');

--inser roles data [MASTER LIST - DO NOT CHANGE]
INSERT INTO roles (name, access_index) VALUES
('Project Manager', 0),
('Team Lead', 1),
('Track Lead', 2),
('Team Member', 2);

--inser users data [IGNITION]
INSERT INTO users (name, email, password, access_code) VALUES
('System Admin', 'admin', 'upPassword', 1);

/*
--inser users data [DUMMY]
INSERT INTO users (name, email, access_code) VALUES
('Sayak Deb', 'sayak.deb@email.com', 1),
('Aman Misra', 'aman.misra@mail.com', 0);

--inser user_roles data [DUMMY]
INSERT INTO user_roles (role_id, group_id, module_id, user_id) VALUES
(1, null, null, 1),
(4, 1, 2, 2),
(3, 1, 6, 2);

--inser tasks data [DUMMY]
INSERT INTO tasks (description, owner_id, publisher_id, days_to_complete) VALUES
('Task for Shopper QAS', 1, 1, 10),
('Task for Middleware PSS', 1, 1, 10),
('Task for CWS', 2, 1, 10);

--inser task_mappings data [DUMMY]
INSERT INTO task_mappings (group_id, module_id, task_id) VALUES
(2, 2, 1),
(1, 5, 2),
(1, 6, 3);

--inser task_status data [DUMMY]
INSERT INTO task_status (task_id, user_id) VALUES
(3, 2);
*/
