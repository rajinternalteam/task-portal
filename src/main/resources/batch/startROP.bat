@echo off
title WHG Resource Portal batch script
echo Welcome to WHG Resource Portal batch script!
echo Make sure CATALINA_HOME, ACTIVEMQ_HOME, JAVA_HOME is set as environment variables.
pause

if exist %CATALINA_HOME% goto startTomcat
goto homeNotSet


:startTomcat
echo CATALINA_HOME found as = %CATALINA_HOME%
echo Starting Tomcat Server...
if not exist "%CATALINA_HOME%\bin\startup.bat" goto batNotFound
call "%CATALINA_HOME%\bin\startup.bat"
if exist %ACTIVEMQ_HOME% goto startAMQ
goto amqHomeNotSet


:startAMQ
echo ACTIVEMQ_HOME found as = %ACTIVEMQ_HOME%
echo Starting ActiveMQ Server...
if not exist "%ACTIVEMQ_HOME%\bin\win64\activemq.bat" goto batNotFound
start call "%ACTIVEMQ_HOME%\bin\win64\activemq.bat"
goto end


:homeNotSet
echo CATALINA_HOME is not set as an Environment Variable.
goto end


:batNotFound
echo startup batch not found.
goto end

:amqHomeNotSet
echo ACTIVEMQ_HOME is not set as an Environment Variable.
goto end


:end
echo Thank You! Now you can close this window.
pause
