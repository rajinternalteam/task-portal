package org.cts.userportal.config;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.cts.userportal.utility.Utils;
import org.h2.jdbcx.JdbcDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


@Configuration
@DependsOn(value="h2Server")
@EnableScheduling
//@EnableTransactionManagement
public class EmbeddedDBPersistService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmbeddedDBPersistService.class);
	private String backUpTime;
	private static final String PRIMARY_BACKUP_FILE_LOC_DRIVE = "C:";
	private static final String BACKUP_FILE_LOC = "\\userportal\\data\\dbsql.sql";
	private static final String PORTAL_DB_NAME = "portalDB";
	
	private String getBackupFile(){
		String file = BACKUP_FILE_LOC;
		if(Files.exists(Paths.get(PRIMARY_BACKUP_FILE_LOC_DRIVE))) file = PRIMARY_BACKUP_FILE_LOC_DRIVE + file;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Backup DB selected at: "+file);
		}
		return file;
	}
	
	@Bean
	public DataSource dataSource() {
		EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder().
				setType(EmbeddedDatabaseType.H2).
				setName(PORTAL_DB_NAME).
				setScriptEncoding("UTF-8");
		
		String backupFile = getBackupFile();
		//check if backup file exists and is valid sql
		//if(Files.exists(Paths.get(BACKUP_FILE_LOC))) {
		if(Files.exists(Paths.get(backupFile))) {
			//String[] restoreArgs = ("-url jdbc:h2:mem:" + PORTAL_DB_NAME + " -user sa -script " + BACKUP_FILE_LOC).split(" ");
			String[] restoreArgs = ("-url jdbc:h2:mem:" + PORTAL_DB_NAME + " -user sa -script " + backupFile).split(" ");
			JdbcDataSource ds = null;
			try {
				org.h2.tools.RunScript.main(restoreArgs);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("DB restore Completed!");
				}
				ds = new JdbcDataSource();
				ds.setURL("jdbc:h2:mem:" + PORTAL_DB_NAME);
				ds.setUser("sa");
			} catch (SQLException e) {
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Error Occurred - ", e);
				}
				return defaultDBLoad(embeddedDatabaseBuilder).build();
			}
			return ds;
		} else {
			return defaultDBLoad(embeddedDatabaseBuilder).build();
		}
	}
	
	private EmbeddedDatabaseBuilder defaultDBLoad(EmbeddedDatabaseBuilder eDBBuilder) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Loading default DB setup for user portal...");
		}
		return eDBBuilder.addScripts("classpath:create-user-portal-db.sql","classpath:insert-user-portal-data.sql");
	}
	
	@Scheduled(fixedRate = 120000)
	private void backupDB() {
		
		try {
			
			String backupFile = getBackupFile();
			
			//String[] backupArgs = ("-url jdbc:h2:mem:" + PORTAL_DB_NAME + " -user sa -script " + BACKUP_FILE_LOC).split(" ");
			String[] backupArgs = ("-url jdbc:h2:mem:" + PORTAL_DB_NAME + " -user sa -script " + backupFile).split(" ");
			org.h2.tools.Script.main(backupArgs);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Mem DB Backup Completed!");
			}
			setBackUpTime("SUCCESS : " + Utils.getCurrentTimeStamp());
			
			/*String[] restoreArgs = "-url jdbc:h2:mem:dataSource -user sa -script \\userportal\\data\\dbsql.zip -options compression zip".split(" ");
			org.h2.tools.RunScript.main(restoreArgs);
			System.out.println("DB restore Completed!");*/
			
		} catch(Exception ex) {
			setBackUpTime("FAIL : " + Utils.getCurrentTimeStamp());
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error Occurred - ", ex);
			}
		}
		
	}


	public String getBackUpTime() {
		return backUpTime;
	}

	private void setBackUpTime(String backUpTime) {
		this.backUpTime = backUpTime;
	}
	
	
}
