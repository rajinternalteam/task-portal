package org.cts.userportal.model;


/**
 * Possible values of Role could be Project Manager, Project Lead, Team lead, General etc.
 * @author Dhiman Mondal
 *
 */
public class Role {

	public enum AccessLevel{
		ProjectLevelAccess(0), GroupLevelAccess(1), ModuleLevelAccess(2);
		private int index;
		public int getIndex() {
			return index;
		}
		AccessLevel(int index) {
			this.index = index;
		}
	}
	
	public Role() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param accessLevelIndex
	 */
	public Role(int id, String name, int accessLevelIndex) {
		super();
		this.id = id;
		this.name = name;
		for(AccessLevel level : AccessLevel.values()){
			if(level.getIndex() == accessLevelIndex){
				setAccessLevel(level);
				break;
			}
		}
	}
	
	private int id;
	private int adminId;
	private String name;
	private AccessLevel accessLevel;
	private int accessLevelIndex;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AccessLevel getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(AccessLevel accessLevel) {
		this.accessLevel = accessLevel;
		this.accessLevelIndex = accessLevel.getIndex();
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", accessLevel="
				+ accessLevel + ", accessLevelIndex=" + accessLevelIndex + "]";
	}

	public int getAccessLevelIndex() {
		return accessLevelIndex;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

}
