package org.cts.userportal.model;

import java.util.Date;

import org.cts.userportal.utility.Utils;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Stores Task status for each user and each task
 * @author Dhiman Mondal
 */
public class TaskStatus {

	public enum Status{
		NotStarted (0), WIP (1), Completed (2), Closed (3);
		private int value;
		Status(int value){
			this.value = value;
		}
		public int getValue() {
			return value;
		}
	}
	
	private int id;
	private Status status;
	private int statusCode;
	private User user;
	private Task task;
	private int taskId;
	
	private Date createdOn;
	private Date lastUpdatedOn;
	private boolean flagged; 
	private int adminId;
	
	public TaskStatus() {
		super();
	}

	/**
	 * @param status
	 * @param user
	 * @param task
	 */
	public TaskStatus(int status, User user, Task task) {
		super();
		this.user = user;
		this.task = task;
		for(Status s : Status.values()){
			if(s.getValue()==status){
				setStatus(s);
				break;
			}
		}
	}
	
	/**
	 * @param id
	 * @param status
	 * @param user
	 * @param taskId
	 */
	public TaskStatus(int id, int taskId, User user, int status) {
		super();
		this.id = id;
		this.user = user;
		this.taskId = taskId;
		for(Status s : Status.values()){
			if(s.getValue()==status){
				setStatus(s);
				break;
			}
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
		this.statusCode = status.getValue();
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Task getTask() {
		return task;
	}
	public void setTask(Task task) {
		this.task = task;
	}

	public int getStatusCode() {
		return statusCode;
	}
	@JsonIgnore
	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "TaskStatus [id=" + id + ", status=" + status + ", statusCode="
				+ statusCode + "]";
	}

	public String getCreatedOn() {
		return Utils.getDisplayDate(this.createdOn);
	}
	
	@JsonIgnore
	public Date getCreatedOnDate() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getLastUpdatedOn() {
		return Utils.getDisplayDate(this.lastUpdatedOn);
	}
	
	@JsonIgnore
	public Date getLastUpdatedOnDate() {
		return this.lastUpdatedOn;
	}

	public void setLastUpdatedOn(Date lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public boolean isFlagged() {
		return flagged;
	}

	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

}
