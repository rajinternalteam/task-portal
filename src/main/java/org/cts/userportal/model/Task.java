package org.cts.userportal.model;

import java.util.List;

/**
 * A Task will have description, owner, publisher, will be for users of certain groups or modules and will have status for each applicable user
 * @author Dhiman Mondal
 *
 */
public class Task {
	
	private int id;
	private String description;
	private String revDescription;
	
	private boolean reverseTask;
	private int reverseId;
	
	private User publisher;
	private User owner;
	
	private List<TaskMapping> mappings;
	private TaskStatus status;
	private List<TaskStatus> allTaskStatus;
	
	private int daysToComplete;
	private boolean flagged;
	
	private int adminId;
	private boolean deleted;
	
	private boolean rePublished;
	
	public Task() {
		super();
	}
	
	/**
	 * @param id
	 * @param description
	 */
	public Task(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @param description
	 * @param publisher
	 * @param owner
	 * @param mappings
	 * @param allStatus
	 */
	public Task(String description, User publisher, User owner,
			List<TaskMapping> mappings, List<TaskStatus> allTaskStatus) {
		super();
		this.description = description;
		this.publisher = publisher;
		this.owner = owner;
		this.mappings = mappings;
		this.allTaskStatus = allTaskStatus;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public User getPublisher() {
		return publisher;
	}
	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<TaskMapping> getMappings() {
		return mappings;
	}

	public void setMappings(List<TaskMapping> mappings) {
		this.mappings = mappings;
	}

	public List<TaskStatus> getAllTaskStatus() {
		return allTaskStatus;
	}

	public void setAllTaskStatus(List<TaskStatus> allTaskStatus) {
		this.allTaskStatus = allTaskStatus;
	}

	@Override
	public String toString() {
		return "Task [id=" + id + ", description=" + description + ",revDescription=" + revDescription
				+ ", publisher=" + publisher + ", owner=" + owner + "]";
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public int getDaysToComplete() {
		return daysToComplete;
	}

	public void setDaysToComplete(int daysToComplete) {
		this.daysToComplete = daysToComplete;
	}

	public boolean isFlagged() {
		return flagged;
	}

	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}

	public String getRevDescription() {
		return revDescription;
	}

	public void setRevDescription(String revDescription) {
		this.revDescription = revDescription;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isReverseTask() {
		return reverseTask;
	}

	public void setReverseTask(boolean reverseTask) {
		this.reverseTask = reverseTask;
	}

	public int getReverseId() {
		return reverseId;
	}

	public void setReverseId(int reverseId) {
		this.reverseId = reverseId;
	}

	public boolean isRePublished() {
		return rePublished;
	}

	public void setRePublished(boolean rePublished) {
		this.rePublished = rePublished;
	}

}
