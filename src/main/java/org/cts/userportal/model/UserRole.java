package org.cts.userportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Syntax of a User Role would be {Role} {Module}_{Group}
 * @author Dhiman Mondal
 *
 */
public class UserRole {
	
	private int id;
	private Role role;
	private Group group;
	private Module module;
	private int userId;
	
	/**
	 * @param id
	 * @param role
	 * @param group
	 * @param module
	 */
	public UserRole(int id, Role role, Group group, Module module) {
		super();
		this.id = id;
		this.group = group;
		this.module = module;
		this.role = role;
	}
	
	/**
	 * @param id
	 * @param role
	 */
	public UserRole(int id, Role role) {
		super();
		this.id = id;
		this.role = role;
	}
	public UserRole() {
		super();
	}
	
	/**
	 * @return User Role e.g. Module Lead CRS/CMOR _PSS
	 */
	public String getName() {
		StringBuilder userRole = new StringBuilder();
		if(role!=null) userRole.append(role.getName());
		if(module!=null) userRole.append(" ").append(module.getName());
		if(group!=null) userRole.append("_").append(group.getName());
		return userRole.toString();
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Module getModule() {
		return module;
	}
	public void setModule(Module module) {
		this.module = module;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@JsonIgnore
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserRole [id=" + id + ", role=" + role + ", group=" + group
				+ ", module=" + module + ", userId=" + userId + "]";
	}
		

}
