package org.cts.userportal.model;


/**
 * Possible values of Module could be CRS/CMOR, SHOPPER, CWS etc. 
 * @author Dhiman Mondal
 *
 */
public class Module {
	
	private int id;
	private String name;
	private int adminId;
	
	/**
	 * @param id
	 * @param name
	 */
	public Module(int id, String name){
		super();
		this.id = id;
		this.name = name;
	}
	
	public Module() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Module [id=" + id + ", name=" + name + "]";
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

}
