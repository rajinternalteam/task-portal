package org.cts.userportal.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A user can have General/Admin access and one or more UserRole to perform
 * @author Dhiman Mondal
 *
 */
public class User {
	
	public enum TaskRole{
		Publisher(0), Owner (1), User (2);
		private int value;
		TaskRole(int value){
			this.value = value;
		}
		public int getValue(){
			return value;
		}
	}

	public enum Access {
		General(0), Admin(1);
		private int code;
		Access(int code){
			this.code = code;
		}
		public int getCode() {
			return code;
		}		
	}
	
	public User() {
		super();
	}
	
	/**
	 * @param id
	 * @param name
	 * @param email
	 */
	public User(int id, String name, String email) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
	}
	
	/**
	 * @param id
	 * @param name
	 * @param email
	 * @param accessCode
	 */
	public User(int id, String name, String email, int accessCode) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		for(Access access : Access.values()){
			if(access.getCode()==accessCode){
				setAccess(access);
				break;
			}
		}
	}

	/**
	 * @param id
	 */
	public User(int id) {
		super();
		this.id = id;
	}

	private int id;	
	private String name;
	private String email;
	private String password;
	
	private Access access;
	private int accessCode;
	
	private List<UserRole> userRoles;
	private Map<TaskRole, List<Task>> tasks;
	
	private int adminId;
	private boolean deleted;
	
	private boolean hasIncompletetasks;
	
	private boolean reBoarded;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email!=null ? email.toLowerCase() : email;
	}

	public void setEmail(String email) {
		this.email = email!=null ? email.toLowerCase() : email;
	}

	public Access getAccess() {
		return access;
	}

	public void setAccess(Access access) {
		this.access = access;
		this.accessCode = access.getCode();
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email
				+ ", access=" + access + ", accessCode=" + accessCode + "]";
	}

	public int getAccessCode() {
		return accessCode;
	}

	public Map<TaskRole, List<Task>> getTasks() {
		return tasks;
	}

	public void setTasks(Map<TaskRole, List<Task>> tasks) {
		this.tasks = tasks;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@JsonIgnore
	public boolean hasIncompletetasks() {
		return hasIncompletetasks;
	}

	public void setIncompletetasks(boolean hasIncompletetasks) {
		this.hasIncompletetasks = hasIncompletetasks;
	}

	public boolean isReBoarded() {
		return reBoarded;
	}

	public void setReBoarded(boolean reBoarded) {
		this.reBoarded = reBoarded;
	}

	@Override
	public boolean equals (Object obj){
		if(obj==this){
			return true;
		}
		if(obj==null || obj.getClass()!=this.getClass()){
			return false;
		}
		User user = (User) obj;
		return this.getId()==user.getId();
	}
	
	@Override
	public int hashCode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}
	
}
