package org.cts.userportal.model;



/**
 * Possible Value of Groups can be PSS, QAS
 * @author Dhiman Mondal
 *
 */
public class Group {
	
	private int id;	
	private String name;
	private int adminId;

	/**
	 * @param id
	 * @param name
	 */
	public Group(int id, String name){
		super();
		this.id = id;
		this.name = name;
	}
	
	public Group() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Group [id=" + id + ", name=" + name + "]";
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

}
