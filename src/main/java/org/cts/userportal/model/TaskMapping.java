package org.cts.userportal.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This class is responsible for mapping of a task with set of groups or modules
 * @author Dhiman Mondal
 */
public class TaskMapping {
	
	private int id;
	private int groupId;
	private String groupName;
	private int moduleId;
	private String moduleName;
	private int taskId;

	public TaskMapping() {
		super();
	}

	/**
	 * @param groupId
	 * @param moduleId
	 * @param taskId
	 */
	public TaskMapping(int groupId, int moduleId, int taskId) {
		super();
		this.groupId = groupId;
		this.moduleId = moduleId;
		this.taskId = taskId;
	}

	/**
	 * @param groupId
	 * @param moduleId
	 * @param groupName
	 * @param moduleName
	 * @param taskId
	 */
	public TaskMapping(int id, int groupId, String groupName, int moduleId, String moduleName, int taskId) {
		super();
		this.id = id;
		this.groupId = groupId;
		this.moduleId = moduleId;
		this.groupName = groupName;
		this.moduleName = moduleName;
		this.taskId = taskId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	@JsonIgnore
	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	@Override
	public String toString() {
		return "TaskMapping [id=" + id + ", groupId=" + groupId
				+ ", groupName=" + groupName + ", moduleId=" + moduleId
				+ ", moduleName=" + moduleName + ", taskId=" + taskId + "]";
	}

}
