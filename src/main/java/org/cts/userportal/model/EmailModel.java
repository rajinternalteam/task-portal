package org.cts.userportal.model;

import java.util.List;

/**
 * 
 * Email Model For User Portal
 * @author Dhiman Mondal
 *
 */
public class EmailModel {

	String from;
	List<String> to;
	List<String> cc;
	String subject;
	String body;
	
	/**
	 * @param to
	 * @param cc
	 * @param subject
	 * @param body
	 */
	public EmailModel(String from, List<String> to, List<String> cc, String subject,
			String body) {
		super();
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.subject = subject;
		this.body = body;
	}
	
	
	public List<String> getTo() {
		return to;
	}
	public List<String> getCc() {
		return cc;
	}
	public String getSubject() {
		return subject;
	}
	public String getBody() {
		return body;
	}
	public String getFrom() {
		return from;
	}
	
	
}
