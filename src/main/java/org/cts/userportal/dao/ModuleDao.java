package org.cts.userportal.dao;

import java.util.List;

import org.cts.userportal.model.Module;

public interface ModuleDao {

	List<Module> getAllModules();

	Module getModuleById(int id);
	
	Module editModuleById(int id, String name);

	String createNewModuleById(String id);

	int deleteModuleById(int id, int adminId);

}
 