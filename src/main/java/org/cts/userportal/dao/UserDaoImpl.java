package org.cts.userportal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.cts.userportal.model.Group;
import org.cts.userportal.model.Module;
import org.cts.userportal.model.Role;
import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskMapping;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.model.User;
import org.cts.userportal.model.UserRole;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 * Provides CRUD support for USERS and USER_ROLES table in the database
 * @author Dhiman Mondal
 *
 */

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<User> getAllUsers(boolean deletedOnly) throws UserPortalSystemException {

		List<User> users = null;

		try {

			if(deletedOnly){
				//Get deleted only
				users = namedParameterJdbcTemplate.query(CommonConstants.SELECT_DELETED_FROM_USERS, new UserMapper());
			}else{
				//Get Users
				users = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_USERS, new UserMapper());
			}
		} catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return users;
	}

	@Override
	public User getUserById(int id) throws UserPortalSystemException {

		User user = null;
		try {

			//Get User
			SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
			user = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_USER_BY_ID, paramSource, new UserMapper());

		}catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return user;
	}


	private class UserRoleMapper implements RowMapper<UserRole> {

		@Override
		public UserRole mapRow(ResultSet rs, int rowNum) throws SQLException {
			UserRole userRole = new UserRole(rs.getInt("id"), new Role(rs.getInt("role_id"), rs.getString("role_name"), rs.getInt("access_index")));
			if(rs.getString("group_name")!=null){
				userRole.setGroup(new Group(rs.getInt("group_id"), rs.getString("group_name")));
			}
			if(rs.getString("module_name")!=null){
				userRole.setModule(new Module(rs.getInt("module_id"), rs.getString("module_name")));
			}
			if(rs.getInt("user_id") > 0){
				userRole.setUserId(rs.getInt("user_id"));
			}
			return userRole;
		}

	}

	private class UserMapper implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getInt("access_code"));
			user.setDeleted(rs.getBoolean("is_deleted"));
			return user;
		}

	}

	@Override
	public List<UserRole> getAllUserRoles() throws UserPortalSystemException {

		List<UserRole> userRoles = null;
		try{
			//Get User Roles
			userRoles = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_USER_ROLES, new UserRoleMapper());
		} catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return userRoles;
	}

	@Override
	public List<UserRole> getUserRolesByUserId(int id) throws UserPortalSystemException {

		List<UserRole> userRoles = null;
		try{
			//Get User Roles
			SqlParameterSource userParamSource = new MapSqlParameterSource("user_id", id);
			userRoles = namedParameterJdbcTemplate.query(CommonConstants.SELECT_USER_ROLES_BY_USER_ID, userParamSource, new UserRoleMapper());
		} catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return userRoles;

	}

	@Override
	public List<Integer> editUserById(User user) throws UserPortalSystemException {
		
		List<Integer> createdTaskIdList = new ArrayList<Integer>();
		try{
			MapSqlParameterSource paramSource = new MapSqlParameterSource();
			int res =0;
			paramSource.addValue("id", user.getId());
			paramSource.addValue("name", user.getName());
			paramSource.addValue("email", user.getEmail());
			paramSource.addValue("access_code", user.getAccessCode());
			paramSource.addValue("admin_id", user.getAdminId());
			
			if(user.isReBoarded()){
				//If re-boarded set is_deleted false
				res = namedParameterJdbcTemplate.update(CommonConstants.REBOARD_USER_BY_ID, paramSource);
			}else{
				res = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_USER_BY_ID, paramSource);
			}
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Total number of user modified " + res);
			}
			if(user.getUserRoles() != null){
				deleteUserRoleById(user);
				createdTaskIdList.addAll(addUserRoleByID(user));
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Modified User Detail "+ user);
			}
		} catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return createdTaskIdList;
	}

	@Override
	public List<Integer> createNewUser(User user) throws UserPortalSystemException {

		List<Integer> createdTaskIdList = new ArrayList<Integer>();
		
		try{
			GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
			MapSqlParameterSource paramSource = new MapSqlParameterSource();
			
			int res =0;
			paramSource.addValue("name", user.getName());
			paramSource.addValue("email", user.getEmail());
			paramSource.addValue("access_code", user.getAccessCode());
			paramSource.addValue("admin_id", user.getAdminId());
			
			res = namedParameterJdbcTemplate.update(CommonConstants.INSERT_ALL_INTO_USERS, paramSource,generatedKeyHolder);
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Generated user Key " + generatedKeyHolder.getKey().intValue());
				LOGGER.debug("Total number of user added " + res);
				LOGGER.debug("New User added " + user);
			}

			user.setId(generatedKeyHolder.getKey().intValue());
			
			//assign User Roles
			if(user.getUserRoles() != null){
				createdTaskIdList.addAll(addUserRoleByID(user));
			}
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Added User Detail "+ user);
			}
			
		} catch (DuplicateKeyException dke){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_DUBLICATE_EMAIL, dke);
		} catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return createdTaskIdList;
	}

	@Override
	public void validateUserRoles(User user) throws UserPortalSystemException{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Inside validateUserRoles() ----->");
		}
		Integer roleId,groupId,moduleId,userId;
		if(user.getUserRoles() != null){
			for(UserRole userRole : user.getUserRoles()){
				roleId=userRole.getRole()!= null?userRole.getRole().getId():null;
				groupId=userRole.getGroup() != null?userRole.getGroup().getId():null;
				moduleId=userRole.getModule() != null ?userRole.getModule().getId():null;
				userId = 0; 
				
				if(roleId != null){
					if(roleId==1){
						try{
							userId = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_PROJECT_MANAGER_USER_ROLE, new MapSqlParameterSource(), Integer.class);
							if(userId > 0 && userId!=user.getId()){
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Project Manager Role is already present in the Database. Throwing Exception");
								}
								throw new UserPortalSystemException("User with Project Manager Role is already present in the Database.");
							}
						}catch(DataAccessException e){}
					}else if(roleId==2 && groupId!=null){
						try{
							userId = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_TEAM_LEAD_USER_ROLE, new MapSqlParameterSource("group_id", groupId), Integer.class);
							if(userId > 0 && userId!=user.getId()){
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Team Lead Role is already present in the Database for this Group. Throwing Exception");
								}
								throw new UserPortalSystemException("User with Team Lead Role for this Group is already present in the Database.");
							}
						}catch(DataAccessException e){}
					}else if(roleId==3 && groupId!=null && moduleId!=null){
						try{
							userId = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_MODULE_LEAD_USER_ROLE, new MapSqlParameterSource("group_id", groupId).addValue("module_id", moduleId), Integer.class);
							if(userId > 0 && userId!=user.getId()){
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Module Lead Role is already present in the Database for this Group and Module. Throwing Exception");
								}
								throw new UserPortalSystemException("User with Module Lead Role for this Group and Module is already present in the Database.");
							}
						}catch(DataAccessException e){}
					}
				}
			}
		}else{
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("User Roles are empty. Throwing Exception");
			}
			throw new UserPortalSystemException("User can't be added without User Roles.");
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Successfully validated all the user roles!");
		}

	}

	private List<Integer> addUserRoleByID(User user){
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Adding User Role by ID with Details: " + user);
		}
		List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
		Integer roleId,groupId,moduleId;

		for(UserRole userRole : user.getUserRoles()){
			roleId=userRole.getRole()!= null?userRole.getRole().getId():null;
			groupId=userRole.getGroup() != null?userRole.getGroup().getId():null;
			moduleId=userRole.getModule() != null ?userRole.getModule().getId():null;
			batchArgs.add(new MapSqlParameterSource("role_id", roleId).addValue("group_id", groupId).addValue("module_id", moduleId).addValue("user_id", user.getId()));
		}

		int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_ALL_INTO_USER_ROLES, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("User Roles added for user:"+user.getId()+"; Roles rows inserted:"+ ((results != null) ? results.length : "0"));
		}

		//assign tasks created for the roles
		return addTaskToUser(user);

	}

	private class TaskMappingRowMapper implements RowMapper<TaskMapping> {

		@Override
		public TaskMapping mapRow(ResultSet rs, int rowNum) throws SQLException {
			TaskMapping mapping = new TaskMapping(0, 0, null, 0, null, rs.getInt("task_id"));
			return mapping;
		}

	}

	private List<Integer> addTaskToUser(User user){

		Set<Integer> masterTaskIdList= new HashSet<Integer>();
		List<Integer> existingTaskIdList = new ArrayList<Integer>();
		List<Integer> newTaskIdList = new ArrayList<Integer>();
		List<Integer> irrelevantTaskIdList = new ArrayList<Integer>();
		//List<Integer> reBoardedTaskIdList = new ArrayList<Integer>();

		MapSqlParameterSource paramSource;
		for(UserRole userRole : user.getUserRoles()){
			List<TaskMapping> mappings = null;
			paramSource = new MapSqlParameterSource();
			if(userRole.getModule() != null && userRole.getGroup()!= null){
				paramSource.addValue("group_id", userRole.getGroup().getId());
				paramSource.addValue("module_id", userRole.getModule().getId());					
				mappings = namedParameterJdbcTemplate.query(CommonConstants.SELECT_TASK_ID_BY_GROUP_ID_AND_MODULE_ID,paramSource, new TaskMappingRowMapper());
				for(TaskMapping singleMappings : mappings){
					masterTaskIdList.add(singleMappings.getTaskId());
				}
			}else if(userRole.getGroup()!= null){
				paramSource.addValue("group_id", userRole.getGroup().getId());paramSource.addValue("group_id", userRole.getGroup().getId());
				mappings = namedParameterJdbcTemplate.query(CommonConstants.SELECT_TASK_ID_BY_GROUP_ID,paramSource, new TaskMappingRowMapper());
				for(TaskMapping singleMappings : mappings){
					masterTaskIdList.add(singleMappings.getTaskId());
				}
			}
		}

		//Get Existing Tasks
		List<Map<String, Object>> existingTasks = namedParameterJdbcTemplate.queryForList(CommonConstants.SELECT_TASK_ID_BY_USER_ID_FROM_TASK_STATUS,
				new MapSqlParameterSource("user_id", user.getId()));
		if(!existingTasks.isEmpty()){
			for(Map<String, Object> taskmap : existingTasks){
				existingTaskIdList.add((Integer) taskmap.get("TASK_ID"));
			}
		}
		
		//Following is commented
		//As now tasks status are deleted on user delete
		//only non closed reverse tasks will be kept on delete
		/*if(user.isReBoarded()){
			//if user re-boarded
			//Switch all master tasks [those are already existing] to NonStarted
			//And Set other existing status as Closed
			reBoardedTaskIdList.addAll(existingTaskIdList);
			reBoardedTaskIdList.retainAll(masterTaskIdList);
			if(reBoardedTaskIdList.size() > 0){
				List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
				for(int taskId : reBoardedTaskIdList){
					batchArgs.add(new MapSqlParameterSource("status", TaskStatus.Status.NotStarted.getValue()).addValue("task_id", taskId).addValue("user_id", user.getId()).addValue("admin_id", user.getAdminId()));
				}
				int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.UPDATE_TASK_STATUS_BY_USER_ID_AND_TASK_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));
				if(LOGGER.isDebugEnabled()) LOGGER.debug("User Tasks Status were restarted for user:"+user.getId()+"; and for tasks of count:"+ ((results != null) ? results.length : "0"));
			}
		}*/

		//Close irrelevant tasks
		irrelevantTaskIdList.addAll(existingTaskIdList);
		irrelevantTaskIdList.removeAll(masterTaskIdList);
		if(irrelevantTaskIdList.size() > 0){
			List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
			for(int taskId : irrelevantTaskIdList){
				batchArgs.add(new MapSqlParameterSource("status", TaskStatus.Status.Closed.getValue()).addValue("task_id", taskId).addValue("user_id", user.getId()).addValue("admin_id", user.getAdminId()));
			}
			int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.UPDATE_TASK_STATUS_BY_USER_ID_AND_TASK_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("User Tasks Status were closed for user:"+user.getId()+"; and for tasks of count:"+ ((results != null) ? results.length : "0"));
			}
		}

		//keep existing tasks un-hurt
		//insert new tasks into task_status table
		newTaskIdList.addAll(masterTaskIdList);
		newTaskIdList.removeAll(existingTaskIdList);
		
		//TODO
		//filter out deleted tasks [provided task mappings are not being deleted on task delete]
		
		if(newTaskIdList.size() > 0){
			List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
			for(int taskId : newTaskIdList){
				batchArgs.add(new MapSqlParameterSource("task_id", taskId).addValue("user_id", user.getId()).addValue("admin_id", user.getAdminId()));
			}
			int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_TASK_STATUS_WITH_TASK_ID_AND_USER_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("User Tasks added for user:"+user.getId()+"; Tasks rows inserted:"+ ((results != null) ? results.length : "0"));
			}
		}
		
		return newTaskIdList;
		
	}

	private void deleteUserRoleById(User user) throws UserPortalSystemException{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Iside delete User Roles");
		}
		SqlParameterSource paramSource = new MapSqlParameterSource("user_id", user.getId());
		int row = namedParameterJdbcTemplate.update(CommonConstants.DELETE_USER_ROLE_BY_USER_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Total User Roles deleted "+ row);
		}
	}

	@Override
	public User login(User credentials) throws UserPortalSystemException {
		User user = null;
		try {
			//Get User by credentials
			SqlParameterSource paramSource = new MapSqlParameterSource("email", credentials.getEmail()).addValue("password", credentials.getPassword());
			user = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_USER_BY_CREDENTIALS, paramSource, new UserMapper());
			if(user.isDeleted()){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Logging in was blocked for the deleted User:"+user.getName());
				}
				throw new UserPortalSystemException("You do not have access to this portal anymore");
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Login successful for user with id:"+user.getId()+" and email:"+user.getEmail());
			}
		}catch(EmptyResultDataAccessException ere){
			throw new UserPortalSystemException("Email or Password is incorrect", ere);
		}catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return user;
	}

	@Override
	//@Transactional
	public User updateCredentials(User user) throws UserPortalSystemException {

		try{
			int rows = 0;
			MapSqlParameterSource params = new MapSqlParameterSource("email", user.getEmail()).addValue("id", user.getId());
			if(!StringUtils.isEmpty(user.getPassword())){
				params.addValue("password", user.getPassword());
				rows = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_USER_CREDENTIALS, params);
			}else{
				rows = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_USER_CREDENTIALS_EMAIL_ONLY, params);
			}
			
			if(rows != 1){
				throw new IncorrectResultSetColumnCountException(1, rows);
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Credentials updated successfully for user with id:"+user.getId()+" and email:"+user.getEmail());
			}
			
		} catch (DuplicateKeyException dke){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_DUBLICATE_EMAIL, dke);
		} catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return user;

	}

	@Override
	public List<Task> deleteUserById(User user, List<Task> allPublishedTasks) throws UserPortalSystemException {
		
		List<Task> reverseTaskList = new ArrayList<Task>();
		
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("id", user.getId()).addValue("admin_id", user.getAdminId());
			int row = namedParameterJdbcTemplate.update(CommonConstants.DELETE_USER_BY_ID, paramSource);
			if(row != 1){
				throw new IncorrectResultSetColumnCountException(1, row);
			}
			
			//cleanup all user roles
			//so that tasks can not auto flow to the deleted users
			//As task auto flow works based on user roles in the user_roles table
			//Otherwise we need to check the user status (i.e. users table traverse) on task assignment tx
			//Also while reactivating a user from flagged status, any old role may conflict with existing user roles
			//User Roles taken by this user will not be provided to any user until unless those are released 
			deleteUserRoleById(user);
			
			//apply off-boarding tasks
			//Get Existing Tasks
			List<TaskStatus> existingTaskStatusList = new ArrayList<TaskStatus>();
			
			List<Map<String, Object>> existingTasks = namedParameterJdbcTemplate.queryForList(CommonConstants.SELECT_TASK_STATUS_BY_USER_ID_FROM_TASK_STATUS,
					new MapSqlParameterSource("user_id", user.getId()));
			if(!existingTasks.isEmpty()){
				TaskStatus status = null;
				for(Map<String, Object> taskmap : existingTasks){
					status = new TaskStatus();
					status.setId((Integer) taskmap.get("id"));
					status.setTaskId((Integer) taskmap.get("task_id"));
					existingTaskStatusList.add(status);
				}
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Existing tasks count for user: "+user.getId()+" = "+existingTaskStatusList.size());
			}
			
			
			//insert reverse tasks
			if(existingTaskStatusList.size() > 0){
				
				/*
				//close all the existing task status
				SqlParameterSource sqlParamSource = new MapSqlParameterSource("status", TaskStatus.Status.Closed.getValue()).addValue("user_id", user.getId()).addValue("admin_id", user.getAdminId());
				int res = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_STATUS_BY_USER_ID, sqlParamSource);
				if(LOGGER.isDebugEnabled()) LOGGER.debug("Existing User Tasks Status were closed for user:"+user.getId()+"; of count:"+ res);
				*/
				
				//Instead of updating to CLOSED, hard delete all existing task status for the user
				SqlParameterSource sqlParamSource = new MapSqlParameterSource("user_id", user.getId());
				int res = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASK_STATUS_BY_USER_ID, sqlParamSource);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Existing User Tasks Status were deleted for user:"+user.getId()+"; of count:"+ res);
				}
				
				
				List<Integer> reverseTaskIdList = new ArrayList<Integer>();
				//filter only the tasks contains reverse task
				for(TaskStatus status : existingTaskStatusList){
					for(Task task : allPublishedTasks){
						if(task.getId() == status.getTaskId() && task.getReverseId()>0){
							reverseTaskIdList.add(task.getReverseId());
							break;
						}
					}
				}
				
				//list out reverse task
				//make sure reverse task is truly present and is a reverse task
				for(int revTaskId : reverseTaskIdList){
					for(Task task : allPublishedTasks){
						if(revTaskId == task.getId() && task.isReverseTask()){
							reverseTaskList.add(task);
							break;
						}
					}
				}
				
				//insert reverse tasks 
				if(reverseTaskList.size() > 0){
					List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
					for(Task task : reverseTaskList){
						batchArgs.add(new MapSqlParameterSource("task_id", task.getId()).addValue("user_id", user.getId()).addValue("admin_id", user.getAdminId()));
					}
					int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_TASK_STATUS_WITH_TASK_ID_AND_USER_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));

					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("User Reverse Tasks added for user:"+user.getId()+"; Reverse Tasks rows inserted:"+ ((results != null) ? results.length : "0"));
					}
				}else{
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("No reverse tasks to add for the user:"+user.getId());
					}
				}
				
			}
			
		}catch(IncorrectResultSetColumnCountException dae){
			throw new UserPortalSystemException("User doesn't exist in the database", dae);
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		
		return reverseTaskList;
		
	}

	@Override
	public User forgotCredentials(User user) throws UserPortalSystemException {
		try {
			//Get User by credentials
			SqlParameterSource paramSource = new MapSqlParameterSource("email", user.getEmail());
			user = namedParameterJdbcTemplate.queryForObject(CommonConstants.FORGOT_USER_CREDENTIALS, paramSource, new UserMapperWithCredentials());
			if(user.isDeleted()){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Logging in was blocked for the deleted User:"+user.getName());
				}
				throw new UserPortalSystemException("You do not have access to this portal anymore");
			}
		}catch(EmptyResultDataAccessException ere){
			throw new UserPortalSystemException("Email is incorrect", ere);
		}catch (DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		
		return user;
	}
	
	private class UserMapperWithCredentials implements RowMapper<User> {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"));
			user.setDeleted(rs.getBoolean("is_deleted"));
			user.setPassword(rs.getString("password"));
			return user;
		}

	}

}

