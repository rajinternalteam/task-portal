package org.cts.userportal.dao;

import java.util.List;

import org.cts.userportal.model.Task;
import org.cts.userportal.model.User;
import org.cts.userportal.model.UserRole;
import org.cts.userportal.utility.UserPortalSystemException;

public interface UserDao {

	List<User> getAllUsers(boolean deletedOnly) throws UserPortalSystemException;

	User getUserById(int id) throws UserPortalSystemException;
	
	List<UserRole> getAllUserRoles() throws UserPortalSystemException;
	
	List<UserRole> getUserRolesByUserId(int id) throws UserPortalSystemException;

	List<Integer> editUserById(User user) throws UserPortalSystemException;

	List<Integer> createNewUser(User user) throws UserPortalSystemException;

	User login(User credentials) throws UserPortalSystemException;

	User updateCredentials(User user) throws UserPortalSystemException;

	void validateUserRoles(User user) throws UserPortalSystemException;

	List<Task> deleteUserById(User user, List<Task> allPublishedTasks) throws UserPortalSystemException;

	User forgotCredentials(User user) throws UserPortalSystemException;

}

