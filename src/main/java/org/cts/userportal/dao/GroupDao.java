package org.cts.userportal.dao;

import java.util.List;

import org.cts.userportal.model.Group;

public interface GroupDao {
	
	List<Group> getAllGroups();
	
	Group getGroupById(int id);

	int deleteGroupById(int id, int adminId);

	Group editGroupById(int id, String name);

	String createNewGroupById(String name);
    
}
