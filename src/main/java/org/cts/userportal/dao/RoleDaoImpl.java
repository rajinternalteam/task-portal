package org.cts.userportal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.cts.userportal.model.Role;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

/**
 * Provides CRUD support for ROLES table in the database
 * @author Dhiman Mondal
 *
 */
@Repository("roleDao") 
public class RoleDaoImpl implements RoleDao {

	private static final Logger LOGGER =LoggerFactory.getLogger(RoleDaoImpl.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<Role> getAllRoles() throws UserPortalSystemException {

		List<Role> roles = null;
		roles = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_ROLES, new RoleRowMapper());
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Total Roles Retrieved:"+(roles != null ? roles.size() : 0));
		}
		return roles;

	}


	@Override
	public Role getRoleById(int id) throws UserPortalSystemException {

		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		Role role = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_ROLE_BY_ID, paramSource, new RoleRowMapper());
		return role;

	}


	private class RoleRowMapper implements RowMapper<Role> {

		@Override
		public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
			Role role = new Role(rs.getInt("id"), rs.getString("name"), rs.getInt("access_index"));
			return role;
		}

	}


	@Override
	public int deleteRoleById(int id, int adminId) throws UserPortalSystemException {
		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		int row=namedParameterJdbcTemplate.update(CommonConstants.DELETE_ROLE_BY_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(row+" Roles deleted by Admin with user Id: "+adminId);
		}
		return row;
	}

	@Override
	public Role editRoleById(Role currentRole) throws UserPortalSystemException {
		Role role=null;
		int id=currentRole.getId();
		String name=currentRole.getName();
		int index=currentRole.getAccessLevelIndex();
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		int res =0;
		paramSource.addValue("name", name);
		paramSource.addValue("id", id);
		paramSource.addValue("access_index", index);
		res=namedParameterJdbcTemplate.update(CommonConstants.UPDATE_ROLE_BY_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Total number of roles modified " + res);
		}
		role = getRoleById(currentRole.getId());
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Modified role Detail "+ role);
		}
		return role;
	}

	@Override
	public Role createNewRoleById(Role currentRole) throws UserPortalSystemException {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		int res =0;
		paramSource.addValue("name", currentRole.getName());
		paramSource.addValue("access_index",currentRole.getAccessLevelIndex());
		res = namedParameterJdbcTemplate.update(CommonConstants.INSERT_ALL_INTO_ROLES, paramSource,generatedKeyHolder);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Generated role Key " + generatedKeyHolder.getKey().intValue());
			LOGGER.info("Total number of roles added " + res);
		}
		currentRole.setId(generatedKeyHolder.getKey().intValue());
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("New role created " + currentRole);
		}
		return currentRole;
	}



}
