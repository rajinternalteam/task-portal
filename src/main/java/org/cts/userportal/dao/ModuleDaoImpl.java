package org.cts.userportal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.cts.userportal.model.Module;
import org.cts.userportal.utility.CommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 * Provides CRUD support for Modules table in the database
 * @author Dhiman Mondal
 *
 */ 
@Repository("moduleDao")
public class ModuleDaoImpl implements ModuleDao {

	private static final Logger LOGGER =LoggerFactory.getLogger(ModuleDaoImpl.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<Module> getAllModules() {
		List<Module> modules = null;
		modules = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_MODULES, new ModuleRowMapper());
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Total Modules Retrieved:"+(modules != null ? modules.size() : 0));
		}
		return modules;
	}

	@Override
	public Module getModuleById(int id) {
		Module module = null;
		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		try{
			module = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_MODULE_BY_ID, paramSource, new ModuleRowMapper());
		}
		catch(EmptyResultDataAccessException ex)
		{
			return null;
		}
		return module;
	}

	@Override
	public int deleteModuleById(int id, int adminId) {
		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		int row=namedParameterJdbcTemplate.update(CommonConstants.DELETE_MODULE_BY_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(row+" Modules deleted by Admin with user Id: "+adminId);
		}
		return row;
	}

	@Override
	public Module editModuleById(int id, String name) {
		int res=0;
		Map<String,String> paramMap=new HashMap<String, String>();
		paramMap.put("id",Integer.valueOf(id).toString());
		paramMap.put("name", name);;
		res=namedParameterJdbcTemplate.update(CommonConstants.UPDATE_MODULE_BY_ID, paramMap);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Edited:"+res);
		}
		Module module=getModuleById(id);
		return module;
	}

	@Override
	public String createNewModuleById(String name) {
		int res=0;
		SqlParameterSource paramSource = new MapSqlParameterSource("name", name);
		res=namedParameterJdbcTemplate.update(CommonConstants.INSERT_ALL_INTO_MODULES, paramSource);
		return (res>0 ? "success" : "failure");
	}


}

final class ModuleRowMapper implements RowMapper<Module>{
	@Override
	public Module mapRow(ResultSet rs, int rowNum) throws SQLException {
		Module module = new Module();
		module.setId(rs.getInt("id"));
		module.setName(rs.getString("name"));
		return module;
	}		
}

