package org.cts.userportal.dao;

import java.util.List;

import org.cts.userportal.model.Role;
import org.cts.userportal.utility.UserPortalSystemException;

public interface RoleDao {

	List<Role> getAllRoles() throws UserPortalSystemException;

	Role getRoleById(int id) throws UserPortalSystemException;

	int deleteRoleById(int id, int adminId) throws UserPortalSystemException;

	Role editRoleById(Role currentRole) throws UserPortalSystemException;

	Role createNewRoleById(Role currentRole) throws UserPortalSystemException;

}
