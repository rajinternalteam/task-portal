package org.cts.userportal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskMapping;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.model.TaskStatus.Status;
import org.cts.userportal.model.User;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.IncorrectResultSetColumnCountException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

@Repository("taskDao")
public class TaskDaoImpl implements TaskDao{

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskDaoImpl.class);
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	} 

	@Override
	public List<Task> getAllTasks(boolean deletedOnly) throws UserPortalSystemException {

		List<Task> tasks = null;
		try{
			if(deletedOnly){
				tasks = namedParameterJdbcTemplate.query(CommonConstants.SELECT_DELETED_FROM_TASKS, new TaskMapper());
			}else{
				tasks = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_TASKS, new TaskMapper());
			}
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return tasks;
	}

	private class TaskMapper implements RowMapper<Task> {

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {

			Task task = new Task(rs.getInt("id"), rs.getString("description"));
			User owner = new User(rs.getInt("owner_id"), rs.getString("owner_name"), rs.getString("owner_email"));
			User publisher = new User(rs.getInt("publisher_id"), rs.getString("publisher_name"), rs.getString("publisher_email"));
			task.setOwner(owner);
			task.setPublisher(publisher);
			task.setDaysToComplete(rs.getInt("days_to_complete"));
			//task.setRevDescription(rs.getString("reverse_description"));
			task.setReverseTask(rs.getBoolean("reverse_task"));
			task.setReverseId(rs.getInt("reverse_id"));
			task.setDeleted(rs.getBoolean("is_deleted"));
			return task;
		}

	}

	@Override
	public List<TaskMapping> getAllTaskMappings() throws UserPortalSystemException {

		List<TaskMapping> mappings = null;

		try{
			mappings = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_TASK_MAPPINGS, new TaskMappingRowMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return mappings;
	}

	private class TaskMappingRowMapper implements RowMapper<TaskMapping> {

		@Override
		public TaskMapping mapRow(ResultSet rs, int rowNum) throws SQLException {
			TaskMapping mapping = new TaskMapping(rs.getInt("id"), rs.getInt("group_id"), rs.getString("group_name"), rs.getInt("module_id"), rs.getString("module_name"), rs.getInt("task_id"));
			return mapping;
		}

	}

	@Override
	public List<TaskStatus> getAllTaskStatus() throws UserPortalSystemException {

		List<TaskStatus> allStatus = null;

		try{
			allStatus = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_TASK_STATUS, new TaskStatusRowMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return allStatus;
	}

	private class TaskStatusRowMapper implements RowMapper<TaskStatus>{

		@Override
		public TaskStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
			TaskStatus status = new TaskStatus(rs.getInt("id"), rs.getInt("task_id"), new User(rs.getInt("user_id"), rs.getString("user_name"), rs.getString("user_email")), rs.getInt("status"));
			status.setCreatedOn(rs.getTimestamp("create_ts"));
			status.setLastUpdatedOn(rs.getTimestamp("update_ts"));
			status.getUser().setDeleted(rs.getBoolean("is_deleted"));
			return status;
		}

	}

	@Override
	public Task getTaskById(int id) throws UserPortalSystemException {

		Task task = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
			task = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_TASK_BY_ID, paramSource, new TaskMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return task;
	}

	@Override
	public List<TaskMapping> getTaskMappingsByTaskId(int id) throws UserPortalSystemException {

		List<TaskMapping> mappings = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("task_id", id);
			mappings = namedParameterJdbcTemplate.query(CommonConstants.SELECT_TASK_MAPPINGS_BY_TASK_ID, paramSource, new TaskMappingRowMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return mappings;
	}

	@Override
	public List<TaskStatus> getTaskStatusByTaskId(int id) throws UserPortalSystemException {

		List<TaskStatus> allStatus = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("task_id", id);
			allStatus = namedParameterJdbcTemplate.query(CommonConstants.SELECT_TASK_STATUS_BY_TASK_ID, paramSource, new TaskStatusRowMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return allStatus;
	}

	@Override
	public List<Task> getUserTasksByUserId(int userId) throws UserPortalSystemException {

		List<Task> userTasks = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("user_id", userId);
			userTasks = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_USER_TASKS_BY_USER_ID, paramSource, new UserTasksMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return userTasks;
	}

	private class UserTasksMapper implements RowMapper<Task>{

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
			Task task = new Task(rs.getInt("id"), rs.getString("description"));
			TaskStatus status = new TaskStatus();
			status.setId(rs.getInt("status_id"));
			int statusCode = rs.getInt("status");
			for(Status s : Status.values()){
				if(s.getValue()==statusCode){
					status.setStatus(s);
					break;
				}
			}
			status.setCreatedOn(rs.getTimestamp("create_ts"));
			status.setLastUpdatedOn(rs.getTimestamp("update_ts"));
			task.setDaysToComplete(rs.getInt("days_to_complete"));
			//task.setRevDescription(rs.getString("reverse_description"));
			task.setReverseTask(rs.getBoolean("reverse_task"));
			task.setReverseId(rs.getInt("reverse_id"));
			task.setStatus(status);
			Utils.flagTaskStatus(task);
			return task;
		}

	}

	@Override
	public List<Task> getOwnerTasksByUserId(int ownerId)
			throws UserPortalSystemException {
		List<Task> tasks = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("owner_id", ownerId);
			tasks = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_OWNER_TASKS_BY_OWNER_ID, paramSource, new OwnerTaskMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return tasks;
	}

	private class OwnerTaskMapper implements RowMapper<Task> {

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {

			Task task = new Task(rs.getInt("id"), rs.getString("description"));
			User publisher = new User(rs.getInt("publisher_id"), rs.getString("publisher_name"), rs.getString("publisher_email"));
			task.setPublisher(publisher);
			task.setDaysToComplete(rs.getInt("days_to_complete"));
			//task.setRevDescription(rs.getString("reverse_description")); //off-boarding task not required here
			task.setReverseTask(rs.getBoolean("reverse_task"));
			task.setReverseId(rs.getInt("reverse_id"));
			return task;
		}

	}

	@Override
	public List<Task> getPublisherTasksByUserId(int publisherId) throws UserPortalSystemException {
		List<Task> tasks = null;
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("publisher_id", publisherId);
			tasks = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_PUBLISHER_TASKS_BY_PUBLISHER_ID, paramSource, new PublisherTaskMapper());
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return tasks;
	}

	private class PublisherTaskMapper implements RowMapper<Task> {

		@Override
		public Task mapRow(ResultSet rs, int rowNum) throws SQLException {

			Task task = new Task(rs.getInt("id"), rs.getString("description"));
			User owner = new User(rs.getInt("owner_id"), rs.getString("owner_name"), rs.getString("owner_email"));
			task.setOwner(owner);
			task.setDaysToComplete(rs.getInt("days_to_complete"));
			//task.setRevDescription(rs.getString("reverse_description")); //off-boarding task not required here
			task.setReverseTask(rs.getBoolean("reverse_task"));
			task.setReverseId(rs.getInt("reverse_id"));
			return task;
		}

	}

	@Override
	public List<Integer> createNewTask(Task task) throws UserPortalSystemException {
		
		List<Integer> assignedUserIds = new ArrayList<Integer>();
		try{
			int res=0;
			GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
			
			
			MapSqlParameterSource paramSource = new MapSqlParameterSource();
			paramSource.addValue("description", task.getDescription());
			paramSource.addValue("owner_id", task.getOwner().getId());
			paramSource.addValue("publisher_id", task.getPublisher().getId());
			paramSource.addValue("days_to_complete", task.getDaysToComplete()>0 ? task.getDaysToComplete() : CommonConstants.DEFAULT_TASK_TO_COMPLETE_IN_DAYS);
			paramSource.addValue("admin_id", task.getAdminId());
			paramSource.addValue("reverse_task", 0);
			
			if(!StringUtils.isEmpty(task.getRevDescription())){
				//insert reverse task
				int reverseId = insertReverseTask(task);
				paramSource.addValue("reverse_id", reverseId);
			}else{
				paramSource.addValue("reverse_id", -1);
			}
			
			//insert original task
			res=namedParameterJdbcTemplate.update(CommonConstants.INSERT_INTO_TASKS, paramSource, generatedKeyHolder);
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Generated user Key " + generatedKeyHolder.getKey().intValue());
				LOGGER.info("Total number of task added " + res);
				LOGGER.info("New Task added " + task);
			}
			
			task.setId(generatedKeyHolder.getKey().intValue());
			if(task.getId() > 0){
				inserIntoTaskMapping(task);
				assignedUserIds.addAll(getUsersForThisTask(task));
				//TODO
				//filter out deleted users [provided user roles are not being deleted on user delete]
				insertTaskStatus(task, assignedUserIds);
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Added Task Detail "+ task);
			}
			
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}

		return assignedUserIds;

	}

	private void insertTaskStatus(Task task, List<Integer> userIds) {

		List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
		for(int userId : userIds){
			batchArgs.add(new MapSqlParameterSource("task_id", task.getId()).addValue("user_id", userId).addValue("admin_id", task.getAdminId()));
		}
		int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_TASK_STATUS_WITH_TASK_ID_AND_USER_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("User Tasks added for users:"+userIds+"; Tasks rows inserted:"+ ((results != null) ? results.length : "0"));
		}

	}

	private List<Integer> updateTaskStatus(Task task, List<Integer> masterUserIds) {

		//fetch existing task status for the task
		List<TaskStatus> taskStatusList = namedParameterJdbcTemplate.query(CommonConstants.SELECT_TASK_ID_AND_STATUS_BY_TASK_ID,
				new MapSqlParameterSource("task_id", task.getId()), new TaskStatusMiniRowMapper());

		List<Integer> existingUserIds = new ArrayList<Integer>();
		List<Integer> irrelevantUserIds = new ArrayList<Integer>();
		List<Integer> newUserIds = new ArrayList<Integer>();

		for(TaskStatus status: taskStatusList){
			existingUserIds.add(status.getUser().getId());
		}

		irrelevantUserIds.addAll(existingUserIds);
		irrelevantUserIds.removeAll(masterUserIds);
		
		//Close the tasks for irrelevant users in status table
		if(irrelevantUserIds.size() > 0){
			List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
			for(int userId : irrelevantUserIds){
				batchArgs.add(new MapSqlParameterSource("status", TaskStatus.Status.Closed.getValue()).addValue("user_id", userId).addValue("task_id", task.getId()).addValue("admin_id", task.getAdminId()));
			}
			int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.UPDATE_TASK_STATUS_BY_USER_ID_AND_TASK_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("User Tasks Status were closed for task:"+task.getId()+"; and for users of count:"+ ((results != null) ? results.length : "0"));
			}
		}

		//keep existing tasks un-hurt
		newUserIds.addAll(masterUserIds);
		newUserIds.removeAll(existingUserIds);
		
		//Insert new users in status table
		if(newUserIds.size() > 0){
			List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
			for(int userId : newUserIds){
				batchArgs.add(new MapSqlParameterSource("task_id", task.getId()).addValue("user_id", userId).addValue("admin_id", task.getAdminId()));
			}
			int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_TASK_STATUS_WITH_TASK_ID_AND_USER_ID, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("User Tasks Status added for task:"+task.getId()+"; Tasks rows inserted:"+ ((results != null) ? results.length : "0"));
			}
		
		}
		return newUserIds;

	}

	private class TaskStatusMiniRowMapper implements RowMapper<TaskStatus>{

		@Override
		public TaskStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
			TaskStatus status = new TaskStatus(rs.getInt("id"), rs.getInt("task_id"), new User(rs.getInt("user_id"), null, null), rs.getInt("status"));
			return status;
		}

	}

	private List<Integer> getUsersForThisTask(Task task) {

		Map<Integer, List<Integer>> groupModules = new HashMap<Integer, List<Integer>>();
		List<Integer> modules;
		for(TaskMapping mapping : task.getMappings()){
			modules = groupModules.get(mapping.getGroupId());
			if(modules != null){
				modules.add(mapping.getModuleId());
			}else{
				modules = new ArrayList<Integer>();
				modules.add(mapping.getModuleId());
			}
			groupModules.put(mapping.getGroupId(), modules);
		}

		Set<Integer> masterUserIds = new HashSet<Integer>();
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		for(Integer groupId : groupModules.keySet()){
			paramMap.put("group_id", groupId);
			paramMap.put("module_ids", groupModules.get(groupId));
			List<Integer> userIds = namedParameterJdbcTemplate.queryForList(CommonConstants.SELECT_DISTINCT_USERID_FROM_USER_ROLE_BY_MODULE_ID_AND_GROUP_ID, paramMap, Integer.class);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Users found for group:"+groupId+" and modules:"+groupModules.get(groupId)+" := "+userIds);
			}
			masterUserIds.addAll(userIds);
		}

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("ALL Users for this tasks:"+masterUserIds);
		}

		return Arrays.asList(masterUserIds.toArray(new Integer[masterUserIds.size()]));

	}

	private void inserIntoTaskMapping(Task task) {
		TaskMapping taskMapping=null;
		List<SqlParameterSource> batchArgs = new ArrayList<SqlParameterSource>();
		List<TaskMapping> allMapping= task.getMappings();
		Integer groupId,moduleId,taskId;
		taskId=task.getId();
		ListIterator<TaskMapping> it=allMapping.listIterator();
		while(it.hasNext())
		{
			taskMapping=it.next();
			groupId= taskMapping.getGroupId();
			taskMapping.setTaskId(taskId);
			moduleId=taskMapping.getModuleId();
			batchArgs.add(new MapSqlParameterSource("group_id", groupId).addValue("module_id", moduleId).addValue("task_id", taskMapping.getTaskId()));
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("mapping to be inserted into task"+ taskMapping.toString());
			}
		}
		int[] results = namedParameterJdbcTemplate.batchUpdate(CommonConstants.INSERT_ALL_INTO_TASKS_MAPPING, batchArgs.toArray(new SqlParameterSource[batchArgs.size()]));

		if(results.length>0){
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No of mapping inserted into task"+ results.length);
			}
		}

	}

	@Override
	public List<Integer> editTaskById(Task task, Integer id) throws UserPortalSystemException {
		
		List<Integer> newUserIds = new ArrayList<Integer>();
		try{
			int res = 0;
			MapSqlParameterSource paramSource = new MapSqlParameterSource();
			paramSource.addValue("id", id);
			paramSource.addValue("description", task.getDescription());
			paramSource.addValue("owner_id", task.getOwner().getId());
			paramSource.addValue("publisher_id", task.getPublisher().getId());
			paramSource.addValue("days_to_complete", task.getDaysToComplete()>0 ? task.getDaysToComplete() : CommonConstants.DEFAULT_TASK_TO_COMPLETE_IN_DAYS);
			paramSource.addValue("admin_id", task.getAdminId());
			paramSource.addValue("reverse_id", task.getReverseId());
			paramSource.addValue("reverse_task", 0);
			
			if(task.getReverseId() > 0){
				//indicates task has existing reverse task 
				if(!StringUtils.isEmpty(task.getRevDescription())){
					//update reverse task
					updateReverseTask(task, task.getReverseId());
				}else{
					//delete reverse task
					deleteReverseTask(task, task.getReverseId());
					paramSource.addValue("reverse_id", -1);
				}
				
			}else if(!StringUtils.isEmpty(task.getRevDescription())){
				//insert reverse task
				int reverseId = insertReverseTask(task);
				paramSource.addValue("reverse_id", reverseId);
			}else{
				paramSource.addValue("reverse_id", -1);
			}
			
			if(task.isRePublished()){
				res=namedParameterJdbcTemplate.update(CommonConstants.REPUBLISH_TASK_BY_ID, paramSource);
			}else{
				res=namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_BY_ID, paramSource);
			}
			
			if(res>0){
				deleteTaskMappingsByTaskId(task);
				inserIntoTaskMapping(task);
				List<Integer> userIds = getUsersForThisTask(task);
				
				//TODO
				//filter out deleted users [provided user roles are not being deleted on user delete]
				
				newUserIds.addAll(updateTaskStatus(task, userIds));
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Added Task Detail "+ task);
			}
			
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return newUserIds;
	}

	private void deleteReverseTask(Task task, int reverseId) {
		
		//delete reverse task
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", task.getReverseId());
		int res = namedParameterJdbcTemplate.update(CommonConstants.DELETE_REVERSE_TASK_BY_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Total number of reverse task deleted= " + res);
		}
		
	}

	private void updateReverseTask(Task task, int reverseId) {
		
		//update reverse task
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("owner_id", task.getOwner().getId());
		paramSource.addValue("publisher_id", task.getPublisher().getId());
		paramSource.addValue("days_to_complete", task.getDaysToComplete()>0 ? task.getDaysToComplete() : CommonConstants.DEFAULT_TASK_TO_COMPLETE_IN_DAYS);
		paramSource.addValue("admin_id", task.getAdminId());
		
		paramSource.addValue("id", task.getReverseId());
		paramSource.addValue("description", task.getRevDescription());
		paramSource.addValue("reverse_task", 1);
		paramSource.addValue("reverse_id", -1);
		
		int res = 0;
		if(task.isRePublished()){
			res=namedParameterJdbcTemplate.update(CommonConstants.REPUBLISH_TASK_BY_ID, paramSource);
		}else{
			res=namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_BY_ID, paramSource);
		}
		
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Total number of reverse task updated= " + res);
		}
		
	}

	private int insertReverseTask(Task task) {
		
		//insert reverse task
		GeneratedKeyHolder generatedReverseKeyHolder = new GeneratedKeyHolder();
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("owner_id", task.getOwner().getId());
		paramSource.addValue("publisher_id", task.getPublisher().getId());
		paramSource.addValue("days_to_complete", task.getDaysToComplete()>0 ? task.getDaysToComplete() : CommonConstants.DEFAULT_TASK_TO_COMPLETE_IN_DAYS);
		paramSource.addValue("admin_id", task.getAdminId());
		
		paramSource.addValue("description", task.getRevDescription());
		paramSource.addValue("reverse_task", 1);
		paramSource.addValue("reverse_id", -1);
		int res = namedParameterJdbcTemplate.update(CommonConstants.INSERT_INTO_TASKS, paramSource, generatedReverseKeyHolder);
		
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Generated reverse task Key " + generatedReverseKeyHolder.getKey().intValue());
			LOGGER.info("Total number of reverse task added " + res);
		}
		
		return generatedReverseKeyHolder.getKey().intValue();
	}

	private void deleteTaskMappingsByTaskId(Task task){
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Iside deleteTaskMappingsByTaskId()");
		}
		SqlParameterSource paramSource = new MapSqlParameterSource("task_id", task.getId());
		int row = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASKS_MAPPING_BY_TASK_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Total Task Mappings deleted "+ row);
		}
	}

	@Override
	public TaskStatus updateTaskStatus(TaskStatus status) throws UserPortalSystemException {
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("id", status.getId())
												.addValue("status", status.getStatusCode())
												.addValue("admin_id", status.getAdminId());
			int rows = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_STATUS_BY_STATUS_ID, paramSource);
			if(rows != 1){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Task Status update was failed, updated row count:"+ rows);
				}
				throw new UserPortalSystemException(new IncorrectResultSetColumnCountException(1, rows));
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Updated Task Status:"+ status);
			}
			
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		return status;
	}

	@Override
	public void deleteTaskById(Task task) throws UserPortalSystemException {
		
		try{
			SqlParameterSource paramSource = new MapSqlParameterSource("id", task.getId()).addValue("admin_id", task.getAdminId());
			int row = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASK_BY_ID, paramSource);
			if(row != 1){
				throw new IncorrectResultSetColumnCountException(1, row);
			}
			
			//cleanup task mappings
			//so that deleted tasks can not auto flow to the users
			//As task auto flow works based on task mappings in  the task_mappings table
			//Otherwise we need to check the task status (i.e. tasks table traverse) on task assignment tx
			deleteTaskMappingsByTaskId(task);
			
			/*
			//close all task status by task id 
			SqlParameterSource statusParamSource = new MapSqlParameterSource("task_id", task.getId()).addValue("status", TaskStatus.Status.Closed.getValue()).addValue("admin_id", task.getAdminId());
			int result = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_STATUS_BY_TASK_ID, statusParamSource);
			if(LOGGER.isDebugEnabled()) LOGGER.debug("Tasks Status are closed for task:"+task.getId()+"; of count:"+ result);
			*/
			
			//instead of closing all task status by task id
			//delete all status by task id
			SqlParameterSource statusParamSource = new MapSqlParameterSource("task_id", task.getId());
			int result = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASK_STATUS_BY_TASK_ID, statusParamSource);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Tasks Status are deleted for task:"+task.getId()+"; of count:"+ result);
			}
			
			
			//delete reverse task as well
			if(task.getReverseId() > 0){
				
				paramSource = new MapSqlParameterSource("id", task.getReverseId()).addValue("admin_id", task.getAdminId());
				row = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASK_BY_ID, paramSource);
				if(row != 1){
					throw new IncorrectResultSetColumnCountException(1, row);
				}
				
				/*
				//close all reverse task status by task id 
				statusParamSource = new MapSqlParameterSource("task_id", task.getReverseId()).addValue("status", TaskStatus.Status.Closed.getValue()).addValue("admin_id", task.getAdminId());
				result = namedParameterJdbcTemplate.update(CommonConstants.UPDATE_TASK_STATUS_BY_TASK_ID, statusParamSource);
				if(LOGGER.isDebugEnabled()) LOGGER.debug("Reverse Tasks Status are closed for reverse task:"+task.getReverseId()+"; of count:"+ result);
				*/
				
				//instead of closing all reverse task status by reverse task id
				//delete all status by reverse task id
				statusParamSource = new MapSqlParameterSource("task_id", task.getReverseId());
				result = namedParameterJdbcTemplate.update(CommonConstants.DELETE_TASK_STATUS_BY_TASK_ID, statusParamSource);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Reverse Tasks Status are deleted for reverse task:"+task.getReverseId()+"; of count:"+ result);
				}
				
			}
			
			
		}catch(IncorrectResultSetColumnCountException dae){
			throw new UserPortalSystemException("Task doesn't exist in the database", dae);
		}catch(DataAccessException dae){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_SQL_EXCEPTION, dae);
		}
		
	}

}
