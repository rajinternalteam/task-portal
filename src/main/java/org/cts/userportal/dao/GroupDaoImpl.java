package org.cts.userportal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.cts.userportal.model.Group;
import org.cts.userportal.utility.CommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 * Provides CRUD support for Groups table in the database
 * @author Dhiman Mondal
 *
 */

@Repository("groupDao")
public class GroupDaoImpl implements GroupDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupDaoImpl.class);

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource){
		this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public List<Group> getAllGroups() {
		List<Group> groups = null;
		groups = namedParameterJdbcTemplate.query(CommonConstants.SELECT_ALL_FROM_GROUPS, new GroupRowMapper());
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Total Groups Retrieved:"+(groups != null ? groups.size() : 0));
		}
		return groups;
	}

	@Override
	public Group getGroupById(int id) {
		Group group = null;
		//SqlParameterSource paramSource1 = new MapSqlParameterSource("id", Integer.valueOf(id));
		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		try{
			group = namedParameterJdbcTemplate.queryForObject(CommonConstants.SELECT_GROUP_BY_ID, paramSource, new GroupRowMapper());
		}
		catch(EmptyResultDataAccessException ex)
		{
			return null;
		}
		return group;
	}

	@Override
	public int deleteGroupById(int id, int adminId) {
		SqlParameterSource paramSource = new MapSqlParameterSource("id", id);
		int row=namedParameterJdbcTemplate.update(CommonConstants.DELETE_GROUP_BY_ID, paramSource);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(row+" Groups deleted by Admin with user Id: "+adminId);
		}
		return row;
	}

	@Override
	public Group editGroupById(int id, String name) {
		int res=0;
		Map<String,String> paramMap=new HashMap<String, String>();
		paramMap.put("id",Integer.valueOf(id).toString());
		paramMap.put("name", name);
		res=namedParameterJdbcTemplate.update(CommonConstants.UPDATE_GROUP_BY_ID, paramMap);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Edited: "+res);
		}
		Group group=getGroupById(id);
		return group;
	}


	@Override
	public String createNewGroupById(String name) {
		int res=0;
		SqlParameterSource paramSource = new MapSqlParameterSource("name",name);
		res=namedParameterJdbcTemplate.update(CommonConstants.INSERT_ALL_INTO_GROUPS, paramSource);
		return (res>0 ? "success" : "failure");
	}




}

final class GroupRowMapper implements RowMapper<Group>{
	@Override
	public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
		Group group = new Group();
		group.setId(rs.getInt("id"));
		group.setName(rs.getString("name"));
		return group;
	}		
}
