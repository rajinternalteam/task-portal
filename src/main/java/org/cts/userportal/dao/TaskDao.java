package org.cts.userportal.dao;

import java.util.List;

import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskMapping;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.utility.UserPortalSystemException;

public interface TaskDao {

	List<Task> getAllTasks(boolean deletedOnly) throws UserPortalSystemException;

	List<TaskMapping> getAllTaskMappings() throws UserPortalSystemException;

	List<TaskStatus> getAllTaskStatus() throws UserPortalSystemException;

	Task getTaskById(int id) throws UserPortalSystemException;

	List<TaskMapping> getTaskMappingsByTaskId(int id) throws UserPortalSystemException;

	List<TaskStatus> getTaskStatusByTaskId(int id) throws UserPortalSystemException;

	List<Task> getUserTasksByUserId(int userId) throws UserPortalSystemException;

	List<Task> getOwnerTasksByUserId(int ownerId) throws UserPortalSystemException;

	List<Task> getPublisherTasksByUserId(int publisherId) throws UserPortalSystemException;

	List<Integer> createNewTask(Task task) throws UserPortalSystemException;

	List<Integer> editTaskById(Task task, Integer id) throws UserPortalSystemException;

	TaskStatus updateTaskStatus(TaskStatus status) throws UserPortalSystemException;

	void deleteTaskById(Task task) throws UserPortalSystemException;
	
}
