package org.cts.userportal.web.controller;

import java.util.List;

import org.cts.userportal.model.RestErrorResponse;
import org.cts.userportal.model.User;
import org.cts.userportal.service.UserService;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles CRUD requests for Users and User Roles
 * @author Dhiman Mondal
 *
 */
@RestController
@RequestMapping("/users")
public class UserRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

	@Autowired
	private UserService userService;


	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getAllUsers(
			@RequestParam(value = "excludeUserRoles", required = false) boolean excludeUserRoles,
			@RequestParam(value = "excludeTasks", required = false) boolean excludeTasks,
			@RequestParam(value = "deletedOnly", required = false) boolean deletedOnly)
	{

		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("---------Get All User Details, excludeUserRoles=" + excludeUserRoles + ", excludeTasks="+excludeTasks +", deletedOnly="+deletedOnly);
		}

		try {
			List<User> users = userService.getAllUsers(excludeUserRoles, excludeTasks, deletedOnly);
			users.remove(new User(1));
			headers.set("message", "success");
			response = new ResponseEntity<List<User>>(users, headers, HttpStatus.OK);
		} catch (UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while Fetching all user details", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getUserById(
			@PathVariable("id") Integer id,
			@RequestParam(value = "excludeUserRoles", required = false) boolean excludeUserRoles,
			@RequestParam(value = "excludeTasks", required = false) boolean excludeTasks
			)
	{

		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("---------Get User Details by id="+id+", excludeUserRoles=" + excludeUserRoles + ", excludeTasks="+excludeTasks);
		}

		try {
			if(id != null){
				User user = userService.getUserById(id, excludeUserRoles, excludeTasks);
				headers.set("message", "success");
				response = new ResponseEntity<User>(user, headers, HttpStatus.OK);
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}
		} catch (UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while Fetching user details by ID", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}

	@RequestMapping(value = "/add/", method = RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object editUserById(@RequestBody User user,
			@RequestParam(value = "modify", required = false) boolean modify){

		Object response = null;
		User currentUser = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(" PUT --------- User details reciver " + user);
		}
		try {	
			if(user.getId() > 0 ){
				currentUser=userService.getUserById(user.getId(), true, true);
				if(currentUser!= null){
					modify=true;
				}
			}
			
			if(user.getId()==1 || "admin".equalsIgnoreCase(user.getEmail())){
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION);
			}
			
			if(!StringUtils.isEmpty(user.getEmail()) && !user.getEmail().endsWith("cognizant.com")){
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_EMAIL_FORMAT_ERROR);
			}
			
			if(modify){
				currentUser= userService.editUserById(user);
			}else{
				currentUser= userService.createNewUser(user);
			}
			headers.set("message", "success");
			response = new ResponseEntity<User>(currentUser, headers, HttpStatus.OK);

		} catch (UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while Fetching Adding/Updating User Details", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}

	@RequestMapping(value="/login/", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object login(@RequestBody User credentials){

		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		
		try{

			if(credentials!=null && !StringUtils.isEmpty(credentials.getEmail()) && !StringUtils.isEmpty(credentials.getPassword())){
				User user = userService.login(credentials);
				headers.set("message", "success");
				response = new ResponseEntity<User>(user, headers, HttpStatus.OK);
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}

		}catch(UserPortalSystemException e){
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Login Error", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}

	@RequestMapping(value="/login/update/", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object updateCredentials(@RequestBody User user){

		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		
		try{

			if( user!=null && user.getId()>0 && !StringUtils.isEmpty(user.getEmail()) ){
				
				if(user.getId()==1 || "admin".equalsIgnoreCase(user.getEmail())){
					throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION);
				}
				
				if(!user.getEmail().endsWith("cognizant.com")){
					throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_EMAIL_FORMAT_ERROR);
				}
				
				User updated = userService.updateCredentials(user);
				headers.set("message", "success");
				response = new ResponseEntity<User>(updated, headers, HttpStatus.OK);
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}

		}catch(UserPortalSystemException e){
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Login Update Error", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}
	
	@RequestMapping(value="/login/forgot/", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object forgotCredentials(@RequestBody User user){

		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		
		try{

			if(user!=null && !StringUtils.isEmpty(user.getEmail())){
				userService.forgotCredentials(user);
				headers.set("message", "Password has been sent to your Registered Email");
				response = new ResponseEntity<User>(user, headers, HttpStatus.OK);
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}

		}catch(UserPortalSystemException e){
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Login Forgot Recover Error", e);
			}
			final RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", e.getMessage());
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;

	}
	
	@RequestMapping(value = "/delete/", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object deleteUserById(@RequestBody User user) {
		
		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			if(user !=null && user.getId() > 0){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Delete User: "+user);
				}
				
				if(user.getId()==1 || "admin".equalsIgnoreCase(user.getEmail())){
					throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION);
				}
				
				if(user.getAdminId()>0 && user.getAdminId()==user.getId()){
					throw new UserPortalSystemException("Own account can't be deleted, must be done by some other Admin");
				}
				
				userService.deleteUserById(user);
				
				//check pending tasks (if any)
				if(user.getTasks()==null){
					headers.set("message", "success");
				}else{
					if(user.hasIncompletetasks()){
						headers.set("errorMessage", "User has tasks to perform, all Owned Tasks must be completed before deregistration");
						headers.set("errorCode", CommonConstants.ERROR_CODE_USER_DELETE_INCOMPLETE_TASKS);
					}else{
						headers.set("errorMessage", "User owns one or more tasks, ownership must be withdrawn before deregistration");
						headers.set("errorCode", CommonConstants.ERROR_CODE_USER_DELETE_OWNED_TASKS);
					}
				}
				response = new ResponseEntity<User>(user, headers, HttpStatus.OK);
				
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}
		} catch (UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Delete User Error", e);
			
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}

}

