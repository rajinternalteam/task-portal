package org.cts.userportal.web.controller;


import java.util.ArrayList;
import java.util.List;

import org.cts.userportal.model.Module;
import org.cts.userportal.service.ModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles CRUD requests for Module 
 * @author Dhiman Mondal
 *
 */

@RestController
@RequestMapping("/modules")
public class ModuleRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ModuleRestController.class);
	
	@Autowired
	ModuleService moduleService;

	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Module>> getAllModules(){
		List<Module> modules = new ArrayList<Module>();
		modules.addAll(moduleService.getAllModules());
		return new ResponseEntity<List<Module>>(modules, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Module> getModuleById(@PathVariable("id") int id){
		Module module = null;
		module = moduleService.getModuleById(id);
		return new ResponseEntity<Module>(module, HttpStatus.OK);
	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Module> deleteModuleById(@PathVariable("id") int id, @RequestParam(value = "adminId", required = false) Integer adminId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Fetching & Deleting Module with id " + id);
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		Module module = null;
		module = moduleService.getModuleById(id);
		if (module == null) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Unable to delete. Module with id " + id + " not found");
			}
			responseHeaders.set("message", "Module does  not Exist");
			return new ResponseEntity<Module>(module,responseHeaders,HttpStatus.NOT_FOUND);
		}
		if(adminId==null) adminId = -1;
		int rowCount=moduleService.deleteModuleById(id, adminId);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Deleted:"+rowCount);
		}
		responseHeaders.set("message", "success");
		return new ResponseEntity<Module>(module,responseHeaders,HttpStatus.OK);
	}

	@RequestMapping(value = "/edit/", method = RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Module> editModuleById(@RequestBody Module currentModule) {
		int id=currentModule.getId();
		String name=currentModule.getName();
		Module module=null;
		String result="";
		HttpHeaders responseHeaders = new HttpHeaders();
		module = moduleService.getModuleById(id);
		if(module!=null) {
			module=moduleService.editModuleById(id,name);
			responseHeaders.set("message", "Module is Modified");
			return new ResponseEntity<Module>(module,responseHeaders,HttpStatus.OK);
		}

		result=moduleService.createNewModuleById(name);
		responseHeaders.set("message", result+" Module is newely Created");
		return new ResponseEntity<Module>(module,responseHeaders,HttpStatus.OK);




	}
}

