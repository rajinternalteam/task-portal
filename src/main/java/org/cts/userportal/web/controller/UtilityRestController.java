package org.cts.userportal.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.cts.userportal.config.EmbeddedDBPersistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/misc")
public class UtilityRestController {
	
	@Autowired
	EmbeddedDBPersistService emService;
	
	@RequestMapping(value="/getBackUpState", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, String>> getBackUpState(){
		Map<String, String> status = new HashMap<String, String>();
		status.put("result",emService.getBackUpTime());
        return new ResponseEntity<Map<String, String>>(status, HttpStatus.OK);
	}
}
