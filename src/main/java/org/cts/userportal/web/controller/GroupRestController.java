
package org.cts.userportal.web.controller;
import java.util.ArrayList;
import java.util.List;

import org.cts.userportal.model.Group;
import org.cts.userportal.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles CRUD requests for GROUP 
 * @author Dhiman Mondal
 *
 */

@RestController
@RequestMapping("/groups")
public class GroupRestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupRestController.class);
	@Autowired
	private GroupService groupService;

	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Group>> getAllGroups(){
		List<Group> groups = new ArrayList<Group>();
		groups.addAll(groupService.getAllGroups());
		return new ResponseEntity<List<Group>>(groups, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Group> getGroupById(@PathVariable("id") int id){
		Group group = null;
		group = groupService.getGroupById(id);
		return new ResponseEntity<Group>(group, HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Group> deleteGroupById(@PathVariable("id") int id, @RequestParam(value = "adminId", required = false) Integer adminId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Fetching & Deleting Group with id " + id);
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		Group group = null;
		group = groupService.getGroupById(id);
		if (group == null) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Unable to delete. User with id " + id + " not found");
			}
			responseHeaders.set("message", "Group does  not Exist");
			return new ResponseEntity<Group>(group,responseHeaders,HttpStatus.NOT_FOUND);
		}
		if(adminId==null) adminId = -1;
		int rowCount=groupService.deleteGroupById(id, adminId);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Deleted:"+rowCount);
		}
		responseHeaders.set("message", "success");
		return new ResponseEntity<Group>(group,responseHeaders,HttpStatus.OK);
	}

	@RequestMapping(value = "/edit/", method = RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Group> editGroupById(@RequestBody Group currentGroup) {
		int id=currentGroup.getId();
		String name=currentGroup.getName();
		String result="";
		Group group=null;
		HttpHeaders responseHeaders = new HttpHeaders();
		group = groupService.getGroupById(id);
		if(group!=null) {
			group=groupService.editGroupById(id,name);
			responseHeaders.set("message", "Group is Modified");
			return new ResponseEntity<Group>(group,responseHeaders,HttpStatus.OK);
		}

		result=groupService.createNewGroupById(name);
		responseHeaders.set("message", result+" Group is newely Created");
		return new ResponseEntity<Group>(group,responseHeaders,HttpStatus.OK);
	}

}

