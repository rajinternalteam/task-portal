package org.cts.userportal.web.controller;

import java.util.List;

import org.cts.userportal.model.RestErrorResponse;
import org.cts.userportal.model.Role;
import org.cts.userportal.service.RoleService;
import org.cts.userportal.utility.UserPortalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles CRUD requests for Roles 
 * @author Dhiman Mondal
 *
 */

@RestController
@RequestMapping("/roles")
public class RoleRestController {
	private static final Logger LOGGER =LoggerFactory.getLogger(RoleRestController.class);

	@Autowired
	private RoleService roleService;

	@RequestMapping(value="/", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getAllRoles(){
		Object response = null;
		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Fetching all role details by ID");
			}
			List<Role> roles = roleService.getAllRoles();
			response = new ResponseEntity<List<Role>>(roles, HttpStatus.OK);
		}catch(UserPortalSystemException  e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while Fetching all role details by ID", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			response = new ResponseEntity<RestErrorResponse>(error, HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Object getRoleById(@PathVariable("id") Integer id){
		Object response = null;
		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Fetching role details by ID");
			}
			Role role = null;
			if(id != null){
				role = roleService.getRoleById(id);
			}
			response = new ResponseEntity<Role>(role, HttpStatus.OK);
		}catch(UserPortalSystemException  e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while Fetching role details by ID", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			response = new ResponseEntity<RestErrorResponse>(error, HttpStatus.BAD_REQUEST);
		}
		return response;

	}
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public Object deleteRoleById(@PathVariable("id") Integer id, @RequestParam(value = "adminId", required = false) Integer adminId){
		Object response=null;
		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Fetching & Deleting Role with id " + id);
			}
			HttpHeaders responseHeaders = new HttpHeaders();
			Role role = null;
			
			if(id!=null)
			{
				role = roleService.getRoleById(id);

			}
			
			if (role == null) {
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Unable to delete. Role with id " + id + " not found");
				}
				responseHeaders.set("message", "Role does  not Exist");
				response = new ResponseEntity<Role>(role,responseHeaders,HttpStatus.NOT_FOUND);
			}else{
				if(adminId==null) adminId = -1;
				int rowCount=roleService.deleteRoleById(id, adminId);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Deleted:"+rowCount);
				}
				responseHeaders.set("message", "success");
				response = new ResponseEntity<Role>(role,responseHeaders,HttpStatus.OK);
			}
			
		}catch(UserPortalSystemException  e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error deleting role details by ID", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			response = new ResponseEntity<RestErrorResponse>(error, HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	@RequestMapping(value = "/edit/", method = RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Object editRoleById(@RequestBody Role currentRole) {
		Object response=null;
		Role role=null;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(" PUT --------- Roles details  " + currentRole);
		}
		Integer id=currentRole.getId();
		try{
			if(id > 0) {
				role = roleService.getRoleById(id);
				if(role!=null){
					role=roleService.editRoleById(currentRole);
					response = new ResponseEntity<Role>(role, HttpStatus.OK);
				}
			}
			else{
				role= roleService.createNewRoleById(currentRole);
				response = new ResponseEntity<Role>(role, HttpStatus.OK);
			}


		}
		catch(UserPortalSystemException  e)
		{

			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while updating role details by ID", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			response = new ResponseEntity<RestErrorResponse>(error, HttpStatus.BAD_REQUEST);

		}
		return response;
	}


}

