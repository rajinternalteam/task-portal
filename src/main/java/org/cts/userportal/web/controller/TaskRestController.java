package org.cts.userportal.web.controller;

import java.util.List;

import org.cts.userportal.model.RestErrorResponse;
import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.service.TaskService;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tasks")
public class TaskRestController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskRestController.class);
	
	@Autowired
	private TaskService taskService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getAllTasks(
			@RequestParam(value = "excludeMappings", required = false) boolean excludeMappings,
			@RequestParam(value = "excludeStatus", required = false) boolean excludeStatus,
			@RequestParam(value = "deletedOnly", required = false) boolean deletedOnly,
			@RequestParam(value = "groupId", required = false) Integer groupId,
			@RequestParam(value = "moduleId", required = false) Integer moduleId,
			@RequestParam(value = "userId", required = false) Integer userId)
	{
		
		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Get All Tasks, excludeMappings="+excludeMappings+" ,excludeStatus="+excludeStatus+", deletedOnly="+deletedOnly);
		}
		
		try{
			
			List<Task> tasks = taskService.getAllTasks(excludeMappings, excludeStatus, deletedOnly);
			headers.set("message", "success");
			response = new ResponseEntity<List<Task>>(tasks, headers, HttpStatus.OK);
			
		} catch (UserPortalSystemException e){
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while getting all tasks.", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object getTaskById(
			@PathVariable("id") Integer id,
			@RequestParam(value = "excludeMappings", required = false) boolean excludeMappings,
			@RequestParam(value = "excludeStatus", required = false) boolean excludeStatus)
	{
		
		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Get Task details by id="+id+", excludeMappings="+excludeMappings+" ,excludeStatus="+excludeStatus);
		}
		
		try{
			
			Task task = taskService.getTaskById(id, excludeMappings, excludeStatus);
			headers.set("message", "success");
			response = new ResponseEntity<Task>(task, headers, HttpStatus.OK);
			
		} catch (UserPortalSystemException e){
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while getting task by ID.", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value = "/add/", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object createTask(@RequestBody Task task, @RequestParam(value = "modify", required = false) boolean modify){

		Object response=null;
		Task currentTask = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(" PUT --------- Task details reciver " + task);
		}
		
		try{
			
			if(task.getOwner().getId()==1 || "admin".equalsIgnoreCase(task.getOwner().getEmail())){
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION);
			}
			
			if(task.getId() > 0)
			{
				currentTask = taskService.getTaskById(task.getId(), true, true);
				if(currentTask!=null) {
					modify=true;
				}
			}
			
			if(modify){
				currentTask= taskService.editTaskById(task,task.getId());
			} else {
				currentTask=taskService.createNewTask(task);
			}
			headers.set("message", "success");
			response = new ResponseEntity<Task>(currentTask, headers, HttpStatus.OK);
		}
		catch(UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while adding/updating task", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;
	}
	
	@RequestMapping(value="/status/", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Object updateTaskStatus(@RequestBody TaskStatus status){
		
		Object response=null;
		TaskStatus updatedStatus = null;
		HttpHeaders headers = new HttpHeaders();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info(" POST --------- Task status to be updated " + status);
		}
		
		try{
			if(status!=null && status.getId()>0)
			{
				updatedStatus= taskService.updateTaskStatus(status);
				headers.set("message", "success");
				response = new ResponseEntity<TaskStatus>(updatedStatus, headers, HttpStatus.OK);
			}
		}catch(UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Error while updating task status.", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}

		return response;
		
	}
	
	@RequestMapping(value = "/delete/", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Object deleteTaskById(@RequestBody Task task) {
		
		Object response = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			if(task !=null && task.getId() > 0){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Delete Task: "+task);
				}
				taskService.deleteTaskById(task);
				headers.set("message", "success");
				response = new ResponseEntity<Task>(task, headers, HttpStatus.OK);
			}else{
				throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE);
			}
		} catch (UserPortalSystemException e) {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Delete Task Error", e);
			}
			RestErrorResponse error = new RestErrorResponse();
			error.setErrorMessage(e.getMessage());
			headers.set("message", "failure");
			response = new ResponseEntity<RestErrorResponse>(error, headers, HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}

}

