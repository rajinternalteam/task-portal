package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.dao.RoleDao;
import org.cts.userportal.model.Role;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Contains business logic related to Roles e.g. Manager, Team Lead etc.
 * @author Dhiman Mondal
 *
 */
@Service("roleService")
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public List<Role> getAllRoles() throws UserPortalSystemException {

		return roleDao.getAllRoles();

	}

	@Override
	public Role getRoleById(int id) throws UserPortalSystemException {

		return roleDao.getRoleById(id);

	}

	@Override
	public int deleteRoleById(int id, int adminId) throws UserPortalSystemException {
		//Restrict deletion of ('Project Manager', 0), ('Team Lead', 1), ('Module Lead', 2),
		if(id==1 || id==2 || id==3){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION+": Project Manager, Team Lead, Module Lead Roles can't be deleted");
		}
		return roleDao.deleteRoleById(id, adminId);
	}

	@Override
	public Role editRoleById(Role currentRole) throws UserPortalSystemException{
		//Restrict editing of ('Project Manager', 0), ('Team Lead', 1), ('Module Lead', 2),
		if(currentRole.getId()==1 || currentRole.getId()==2 || currentRole.getId()==3){
			throw new UserPortalSystemException(CommonConstants.ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION+": Project Manager, Team Lead, Module Lead Roles can't be Modified by any User/Admin");
		}
		return roleDao.editRoleById(currentRole);
	}

	@Override
	public Role createNewRoleById(Role currentRole) throws UserPortalSystemException {
		return roleDao.createNewRoleById(currentRole);
	}


}
