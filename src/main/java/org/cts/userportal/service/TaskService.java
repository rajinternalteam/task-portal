package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.utility.UserPortalSystemException;

public interface TaskService {

	List<Task> getAllTasks(boolean excludeMappings, boolean excludeStatus, boolean deletedOnly) throws UserPortalSystemException;

	Task getTaskById(int id, boolean excludeMappings, boolean excludeStatus) throws UserPortalSystemException;
	
	Task createNewTask(Task task) throws UserPortalSystemException;

	Task editTaskById(Task task, Integer id) throws UserPortalSystemException;

	TaskStatus updateTaskStatus(TaskStatus status) throws UserPortalSystemException;

	void deleteTaskById(Task task) throws UserPortalSystemException;
	
}
