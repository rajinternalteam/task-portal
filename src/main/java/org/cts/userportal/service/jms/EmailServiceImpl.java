package org.cts.userportal.service.jms;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;

import org.cts.userportal.model.EmailModel;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

@Service("emailService")
public class EmailServiceImpl implements EmailService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class); 
	
	@Autowired
	ThreadPoolTaskExecutor taskExecutor;
	
	@Autowired
	WebApplicationContext context;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	
	private static final boolean ACTIVE_MQ_ENABLED = true;
	
	@Override
	public void sendMail(String from, List<String> to, List<String> cc, String subject, String body) {
		
		
		try {
			
			if(to == null || StringUtils.isEmpty(subject) || StringUtils.isEmpty(body)){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Returning without Sending Email - Required data no available");
				}
				return;
			}
			
			if(StringUtils.isEmpty(from)){
				from = "whguserportal@cognizant.com";//"donotreply@whguserportal.com";
			}
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Sending Email to: "+to+" with subject: "+subject);
			}
			
			if(ACTIVE_MQ_ENABLED){
				
				StringBuilder buildTo = new StringBuilder();
				StringBuilder buildCc = new StringBuilder();
				
				for(int i=0; i<to.size(); i++){
					if(i!=0) buildTo.append(";");
					buildTo.append(to.get(i));
				}
				
				if(cc != null){
					for(int i=0; i<cc.size(); i++){
						if(i!=0) buildCc.append(";");
						buildCc.append(cc.get(i));
					}
				}
				
				final String emailTo = buildTo.toString();
				final String emailCc = buildCc.toString();
				final String emailFrom = from;
				final String emailBody = body;
				final String emailSubject = subject;
				
				MessageCreator messageCreator = new MessageCreator() {
					
					public Message createMessage(Session session) throws JMSException {
						
						TextMessage message = session.createTextMessage(emailBody);
						message.setStringProperty(CommonConstants.EMAIL_SUBJECT, emailSubject);
						message.setStringProperty(CommonConstants.EMAIL_TO, emailTo);
						
						if(!StringUtils.isEmpty(emailCc)){
							message.setStringProperty(CommonConstants.EMAIL_CC, emailCc);
						}
						
						message.setStringProperty(CommonConstants.EMAIL_FROM, emailFrom);
						
						return message;
					}
				};
				
				jmsTemplate.send(messageCreator);
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Email pushed to Queue successfully!");
				}
				
			}else{
				
				SendEmailTask sendEmailTask = (SendEmailTask) context.getBean("sendEmailTask", SendEmailTask.class);

				if(sendEmailTask != null){

					sendEmailTask.setMessage(from, to, cc, subject, body);

					taskExecutor.execute(sendEmailTask);

				}
				
			}
			
		} catch (MessagingException me) {
			
			if(me!=null && me instanceof SendFailedException){
				List<String> unsent = new ArrayList<String>();
				unsent.addAll(Utils.parseAddress(((SendFailedException)me).getInvalidAddresses()));
				unsent.addAll(Utils.parseAddress(((SendFailedException)me).getValidUnsentAddresses()));
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Sending Mail operation was failed for one or more Users: " + unsent);
				}
			}
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Exception occured while sending mail", me);
			}
			
		} catch (JmsException jmse){
			
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Exception occured while sending mail", jmse);
			}
			
		} catch (Exception e) {
			
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Exception occured while sending mail", e);
			}
			
		}
		
	}


	@Override
	public void sendMail(List<EmailModel> messages) {
		
		for(EmailModel eMsg : messages){
			sendMail(eMsg.getFrom(), eMsg.getTo(), eMsg.getCc(), eMsg.getSubject(), eMsg.getBody());
		}
		
	}
	
	

}
