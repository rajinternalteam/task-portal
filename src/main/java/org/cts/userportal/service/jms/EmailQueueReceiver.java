package org.cts.userportal.service.jms;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailQueueReceiver implements MessageListener{
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	JavaMailSender mailSender;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailQueueReceiver.class);
	
	@Override
	public void onMessage(final Message message) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Message Received from message queue!");
		}
		
		if (message instanceof TextMessage) {
			
			TextMessage queuemessage = (TextMessage)message;
			try {

				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("EmailQueueReceiver will wait for 5 seconds before sending the message");
				}
				Thread.sleep(5000);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("EmailQueueReceiver Ready to send the message!");
				}

				sendEmail(queuemessage);

			} catch (InterruptedException ie) {
				
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Thread interrupted in EmailQueueReceiver: ", ie);
				}
				
			} catch (Exception ex) {
				
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Exception occured while sending mail", ex);
				}
				
				try {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("EmailQueueReceiver will wait for 10 more seconds before retrying");
					}
					Thread.sleep(10000);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("EmailQueueReceiver Ready to resend the message!");
					}
					sendEmail(queuemessage);
				} catch (InterruptedException ie) {

					if(LOGGER.isErrorEnabled()) {
						LOGGER.error("Thread interrupted in EmailQueueReceiver: ", ie);
					}

				} catch (Exception ex1) {
					
					if(LOGGER.isErrorEnabled()) {
						LOGGER.error("Exception occured while resending mail", ex1);
					}
					
					//re-submit in the queue 
					/*jmsTemplate.send(new MessageCreator() {
						@Override
						public Message createMessage(Session arg0) throws JMSException {
							return message;
						}
					});*/
					
				}

			} finally {

				try {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("EmailQueueReceiver will wait for 20 seconds before receiving next message");
					}
					Thread.sleep(20000);
				} catch (InterruptedException ie) {
					if(LOGGER.isErrorEnabled()) {
						LOGGER.error("Thread interrupted in EmailQueueReceiver: ", ie);
					}
				}
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("EmailQueueReceiver Ready to receive next message!");
				}

			}
		}
		
	}
	
	private void sendEmail(TextMessage queuemessage) throws JMSException, MessagingException {

		try {
			
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
			mimeMessageHelper.setFrom(queuemessage.getStringProperty(CommonConstants.EMAIL_FROM));
			
			if(queuemessage.getStringProperty(CommonConstants.EMAIL_TO).contains(";")){
				mimeMessageHelper.setTo(queuemessage.getStringProperty(CommonConstants.EMAIL_TO).split(";"));
			}else{
				mimeMessageHelper.setTo(queuemessage.getStringProperty(CommonConstants.EMAIL_TO));
			}
			
			if(queuemessage.getStringProperty(CommonConstants.EMAIL_CC) != null ){
				if(queuemessage.getStringProperty(CommonConstants.EMAIL_CC).contains(";")){
					mimeMessageHelper.setCc(queuemessage.getStringProperty(CommonConstants.EMAIL_CC).split(";"));
				}else{
					mimeMessageHelper.setCc(queuemessage.getStringProperty(CommonConstants.EMAIL_CC));
				}
			}

			mimeMessageHelper.setSubject(queuemessage.getStringProperty(CommonConstants.EMAIL_SUBJECT));
			mimeMessageHelper.setText(((TextMessage) queuemessage).getText(), true);
			
			mailSender.send(mimeMessage);
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Email has been sent to: "+Utils.parseAddress(mimeMessage.getRecipients(RecipientType.TO))+" Successfully!");
			}
			
		} catch (MessagingException me) {
			
			if(me!=null && me instanceof SendFailedException){
				List<String> unsent = new ArrayList<String>();
				unsent.addAll(Utils.parseAddress(((SendFailedException)me).getInvalidAddresses()));
				unsent.addAll(Utils.parseAddress(((SendFailedException)me).getValidUnsentAddresses()));
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Sending Mail operation was failed for one or more Users: " + unsent);
				}
			}
			throw me;
			
		}
		
	}

}
