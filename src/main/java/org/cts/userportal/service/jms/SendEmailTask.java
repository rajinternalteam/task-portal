package org.cts.userportal.service.jms;

import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class SendEmailTask implements Runnable{

	private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailTask.class);
	
	@Autowired
	JavaMailSender mailSender;
	
	private MimeMessage message;
	
	public void setMessage(String from, List<String> to, List<String> cc, String subject, String body) throws MessagingException {
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
		mimeMessageHelper.setFrom(from);
		mimeMessageHelper.setTo(to.toArray(new String[to.size()]));
		
		if(cc!=null && cc.size()>0){
			mimeMessageHelper.setCc(cc.toArray(new String[cc.size()]));
		}

		mimeMessageHelper.setSubject(subject);
		mimeMessageHelper.setText(body, true);
		this.message = mimeMessage;
	}
	
	public void setMessage(String from, String to, String cc, String subject, String body) throws MessagingException {
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
		mimeMessageHelper.setFrom(from);
		mimeMessageHelper.setTo(to.split(";"));
		
		if(cc!=null){
			mimeMessageHelper.setCc(cc.split(";"));
		}

		mimeMessageHelper.setSubject(subject);
		mimeMessageHelper.setText(body, true);
		this.message = mimeMessage;
	}

	@Override
	public void run() {

		try {
		
			if(this.message != null){
				mailSender.send(this.message);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Email has been sent to: "+Utils.parseAddress(this.message.getRecipients(RecipientType.TO))+" Successfully!");
				}
			}
			
		} catch (MailException e) {
			
			Utils.logExceptions(e);
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Exception occured while sending mail", e);
			}
			
		} catch (Exception e) {
			
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("Exception occured while sending mail", e);
			}
			
		}
	}
	
}
