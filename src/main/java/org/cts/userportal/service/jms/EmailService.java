package org.cts.userportal.service.jms;

import java.util.List;

import org.cts.userportal.model.EmailModel;

public interface EmailService {

	public void sendMail(String from, List<String> to, List<String> cc, String subject, String body);

	public void sendMail(List<EmailModel> messages);
	
}
