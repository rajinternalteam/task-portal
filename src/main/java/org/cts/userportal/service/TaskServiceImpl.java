package org.cts.userportal.service;

import java.util.ArrayList;
import java.util.List;

import org.cts.userportal.dao.TaskDao;
import org.cts.userportal.dao.UserDao;
import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskMapping;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.model.User;
import org.cts.userportal.service.jms.EmailService;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("taskService")
public class TaskServiceImpl implements TaskService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskServiceImpl.class);
	
	@Autowired
	TaskDao taskDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	EmailService emailService;
	
	@Override
	public List<Task> getAllTasks(boolean excludeMappings, boolean excludeStatus, boolean deletedOnly)
			throws UserPortalSystemException {
		
		List<Task> tasks = null;
		
		tasks = taskDao.getAllTasks(deletedOnly);
		
		if(!excludeMappings){
			List<TaskMapping> allMappings = taskDao.getAllTaskMappings();
			
			for(Task task : tasks){
				List<TaskMapping> mappings = new ArrayList<TaskMapping>();
				for(TaskMapping mapping : allMappings){
					if(task.getId()==mapping.getTaskId()) mappings.add(mapping);
				}
				task.setMappings(mappings);
			}
			
		}
		
		if(!excludeStatus){
			
			List<TaskStatus> allStatus = taskDao.getAllTaskStatus();
			for(Task task : tasks){
				List<TaskStatus> statusList = new ArrayList<TaskStatus>();
				for(TaskStatus status : allStatus){
					if(task.getId()==status.getTaskId()) statusList.add(status);
				}
				task.setAllTaskStatus(statusList);
				Utils.flagTaskStatus(task);
			}
			
		}
		
		if(LOGGER.isDebugEnabled() && tasks!=null){
			LOGGER.debug("---------------Tasks Retrieved--------------");
			for(Task task: tasks){
				LOGGER.debug(task.toString());
				if(task.getMappings()!=null && task.getMappings().size()>0){
					LOGGER.debug("--------Task Mappings--------");
					for(TaskMapping mapping : task.getMappings()){
						LOGGER.debug(mapping.toString());
					}
				}
				if(task.getAllTaskStatus()!=null && task.getAllTaskStatus().size()>0){
					LOGGER.debug("--------All Task status--------");
					for(TaskStatus status : task.getAllTaskStatus()){
						LOGGER.debug(status.toString());
					}
				}
				LOGGER.debug("--------------------------------------------");
			}
		}
		
		return tasks;
	}

	@Override
	public Task getTaskById(int id, boolean excludeMappings,
			boolean excludeStatus) throws UserPortalSystemException {
		
		Task task = taskDao.getTaskById(id);
		
		//get reverse task info if available
		if(task!=null && task.getReverseId() > 0){
			Task reverseTask = taskDao.getTaskById(task.getReverseId());
			task.setRevDescription(reverseTask.getDescription());
		}
		
		if(!excludeMappings && task!=null){
			
			List<TaskMapping> mappings = taskDao.getTaskMappingsByTaskId(id);
			task.setMappings(mappings);
			
		}
		
		if(!excludeStatus && task!=null){
			
			List<TaskStatus> allStatus = taskDao.getTaskStatusByTaskId(id);
			task.setAllTaskStatus(allStatus);
			Utils.flagTaskStatus(task);
			
		}
		
		if(LOGGER.isDebugEnabled() && task!=null){
			LOGGER.debug("---------------Task Retrieved--------------");
			LOGGER.debug(task.toString());
			if(task.getMappings()!=null && task.getMappings().size()>0){
				LOGGER.debug("--------Task Mappings--------");
				for(TaskMapping mapping : task.getMappings()){
					LOGGER.debug(mapping.toString());
				}
			}
			if(task.getAllTaskStatus()!=null && task.getAllTaskStatus().size()>0){
				LOGGER.debug("--------All Task status--------");
				for(TaskStatus status : task.getAllTaskStatus()){
					LOGGER.debug(status.toString());
				}
			}
			LOGGER.debug("--------------------------------------------");
		}
		
		return task;
	}

	@Override
	public Task createNewTask(Task task) throws UserPortalSystemException {
		
		List<Integer> assignedUserIds = taskDao.createNewTask(task);
		
		//shoot mail to the task owner
		if(assignedUserIds!=null && assignedUserIds.size()>0){
			List<User> allUsers = userDao.getAllUsers(false);
			List<User> assignedUsers = new ArrayList<User>();
			
			for(int userId : assignedUserIds){
				for(User user : allUsers){
					if(userId == user.getId()){
						assignedUsers.add(user);
						break;
					}
				}
			}
			
			List<String> to = null;
			if(!StringUtils.isEmpty(task.getOwner().getEmail())){
				to = new ArrayList<String>();
				to.add(task.getOwner().getEmail());
			}
			emailService.sendMail(null, to, null, CommonConstants.EMAIL_SUBJECT_NEW_ASSIGNED_TASK, Utils.getEmailBodyForNewTask(assignedUsers, task, true));
			
		}
		
		if(LOGGER.isDebugEnabled() && task!=null){
			LOGGER.debug("---------------Tasks Created--------------");
			LOGGER.debug(task.toString());
			if(task.getMappings()!=null && task.getMappings().size()>0){
				LOGGER.debug("--------Task Mappings--------");
				for(TaskMapping mapping : task.getMappings()){
					LOGGER.debug(mapping.toString());
				}
			}
			if(task.getAllTaskStatus()!=null && task.getAllTaskStatus().size()>0){
				LOGGER.debug("--------All Task status--------");
				for(TaskStatus status : task.getAllTaskStatus()){
					LOGGER.debug(status.toString());
				}
			}
			LOGGER.debug("--------------------------------------------");
		}
		
		return task;
	}

	@Override
	public Task editTaskById(Task task, Integer id) throws UserPortalSystemException {
		
		List<Integer> assignedUserIds = taskDao.editTaskById(task,id);
		
		//shoot mail to the owner if new users
		if(assignedUserIds!=null && assignedUserIds.size()>0){
			List<User> allUsers = userDao.getAllUsers(false);
			List<User> assignedUsers = new ArrayList<User>();
			
			for(int userId : assignedUserIds){
				for(User user : allUsers){
					if(userId == user.getId()){
						assignedUsers.add(user);
						break;
					}
				}
			}
			
			List<String> to = null;
			if(!StringUtils.isEmpty(task.getOwner().getEmail())){
				to = new ArrayList<String>();
				to.add(task.getOwner().getEmail());
			}
			
			emailService.sendMail(null, to, null, CommonConstants.EMAIL_SUBJECT_NEW_ASSIGNED_TASK, Utils.getEmailBodyForNewTask(assignedUsers, task, false));
			
		}
		
		if(LOGGER.isDebugEnabled() && task!=null){
			LOGGER.debug("---------------Updated Task Details--------------");
			LOGGER.debug(task.toString());
			if(task.getMappings()!=null && task.getMappings().size()>0){
				LOGGER.debug("--------Task Mappings--------");
				for(TaskMapping mapping : task.getMappings()){
					LOGGER.debug(mapping.toString());
				}
			}
			if(task.getAllTaskStatus()!=null && task.getAllTaskStatus().size()>0){
				LOGGER.debug("--------All Task status--------");
				for(TaskStatus status : task.getAllTaskStatus()){
					LOGGER.debug(status.toString());
				}
			}
			LOGGER.debug("--------------------------------------------");
		}
		
		return task;
	}

	@Override
	public TaskStatus updateTaskStatus(TaskStatus status) throws UserPortalSystemException {
		return taskDao.updateTaskStatus(status);
	}

	@Override
	public void deleteTaskById(Task task) throws UserPortalSystemException {
		
		taskDao.deleteTaskById(task);
		if(LOGGER.isDebugEnabled() && task!=null){
			LOGGER.debug("Task successfully Deleted: "+task);
			if(task.getMappings()!=null && task.getMappings().size()>0){
				LOGGER.debug("--------Task Mappings Post Delete--------");
				for(TaskMapping mapping : task.getMappings()){
					LOGGER.debug(mapping.toString());
				}
			}
			if(task.getAllTaskStatus()!=null && task.getAllTaskStatus().size()>0){
				LOGGER.debug("--------All Task status Post Delete--------");
				for(TaskStatus status : task.getAllTaskStatus()){
					LOGGER.debug(status.toString());
				}
			}
			LOGGER.debug("--------------------------------------------");
		}
		
	}
	
	
}
