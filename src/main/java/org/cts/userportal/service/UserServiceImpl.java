package org.cts.userportal.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cts.userportal.dao.TaskDao;
import org.cts.userportal.dao.UserDao;
import org.cts.userportal.model.EmailModel;
import org.cts.userportal.model.Task;
import org.cts.userportal.model.TaskStatus;
import org.cts.userportal.model.TaskStatus.Status;
import org.cts.userportal.model.User;
import org.cts.userportal.model.User.TaskRole;
import org.cts.userportal.model.UserRole;
import org.cts.userportal.service.jms.EmailService;
import org.cts.userportal.utility.CommonConstants;
import org.cts.userportal.utility.UserPortalSystemException;
import org.cts.userportal.utility.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Contains business logic related to Users and User Roles
 * @author Dhiman Mondal
 *
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	TaskDao taskDao;
	
	@Autowired
	EmailService emailService;
	
	@Override
	public List<User> getAllUsers(boolean excludeUserRoles, boolean excludeTasks, boolean deletedOnly) throws UserPortalSystemException {
		
		List<User> users = userDao.getAllUsers(deletedOnly);
		
		if(!excludeUserRoles){
			
			List<UserRole> userRoles = userDao.getAllUserRoles();
			
			//Filter User Roles
			for(User user : users){
				List<UserRole> roles = new ArrayList<UserRole>();
				for(UserRole role : userRoles){
					if(user.getId()==role.getUserId()){
						roles.add(role);
					}
				}
				user.setUserRoles(roles);
			}
			
		}
		
		if(!excludeTasks){
			// Fetch Tasks
			for(User user : users){
				retrieveAllTypesOfTasksWithStatus(user);
			}
		}
		
		if(LOGGER.isDebugEnabled() && users!=null){
			LOGGER.debug("---------User Details retrieved------------");
			for(User user : users){
				LOGGER.debug(user.toString());
				if(user.getUserRoles()!=null && user.getUserRoles().size()>0){
					LOGGER.debug("---------User Roles------------");
					for(UserRole ur : user.getUserRoles()){
						LOGGER.debug(ur.toString());
					}
				}
				if(user.getTasks()!=null && user.getTasks().size()>0){
					for(Entry<User.TaskRole,List<Task>> entry : user.getTasks().entrySet()){
						LOGGER.debug("---------"+entry.getKey()+" Tasks------------");
						for(Task task : entry.getValue()){
							LOGGER.debug(task.toString());
						}
					}
				}
				LOGGER.debug("-------------------------------------------");
			}
		}
		
		return users;
		
	}

	@Override
	public User getUserById(int id, boolean excludeUserRoles, boolean excludeTasks) throws UserPortalSystemException {
		
		User user = userDao.getUserById(id);
		
		if(!excludeUserRoles){
			//Get Roles
			user.setUserRoles(userDao.getUserRolesByUserId(id));
			
		}
		
		if(!excludeTasks){
			//Get Tasks
			retrieveAllTypesOfTasksWithStatus(user);
			
		}

		if(LOGGER.isDebugEnabled() && user!=null){
			LOGGER.debug("--------User details retrieved: "+user);
			LOGGER.debug(user.toString());
			if(user.getUserRoles()!=null && user.getUserRoles().size()>0){
				LOGGER.debug("---------User Roles------------");
				for(UserRole ur : user.getUserRoles()){
					LOGGER.debug(ur.toString());
				}
			}
			if(user.getTasks()!=null && user.getTasks().size()>0){
				for(Entry<User.TaskRole,List<Task>> entry : user.getTasks().entrySet()){
					LOGGER.debug("---------"+entry.getKey()+" Tasks------------");
					for(Task task : entry.getValue()){
						LOGGER.debug(task.toString());
					}
				}
			}
			LOGGER.debug("-------------------------------------------");
		}
		
		return user;
	}
	
	private void retrieveAllTypesOfTasksWithStatus(User user) throws UserPortalSystemException{
		
		Map<TaskRole, List<Task>> tasks = new HashMap<TaskRole, List<Task>>();
		int id = user.getId();
		
		List<Task> userTasks = taskDao.getUserTasksByUserId(id);
		List<Task> ownerTasks = taskDao.getOwnerTasksByUserId(id);
		List<Task> publisherTasks = taskDao.getPublisherTasksByUserId(id);
		
		//Get All Task Status
		List<TaskStatus> allStatus = taskDao.getAllTaskStatus();
		
		//Filter status for owner tasks
		for(Task task : ownerTasks){
			List<TaskStatus> statusList = new ArrayList<TaskStatus>();
			for(TaskStatus status : allStatus){
				if(task.getId() == status.getTaskId()
					&& !status.getStatus().equals(Status.Closed) //remove Closed task from the list
					){
					statusList.add(status);
				}
			}
			task.setAllTaskStatus(statusList);
			Utils.flagTaskStatus(task);
		}
		
		//Filter status for publisher tasks
		for(Task task : publisherTasks){
			List<TaskStatus> statusList = new ArrayList<TaskStatus>();
			for(TaskStatus status : allStatus){
				if(task.getId() == status.getTaskId()
					&& !status.getStatus().equals(Status.Closed) //remove Closed task from the list
					){
					statusList.add(status);
				}
			}
			task.setAllTaskStatus(statusList);
			Utils.flagTaskStatus(task);
		}
		
		tasks.put(TaskRole.User, userTasks);
		tasks.put(TaskRole.Owner, ownerTasks);
		tasks.put(TaskRole.Publisher, publisherTasks);
		
		user.setTasks(tasks);
		
	}

	@Override
	public User editUserById(User user) throws UserPortalSystemException {
		
		List<Task> createdTaskList = new ArrayList<Task>();
		
		userDao.validateUserRoles(user);
		
		List<Integer> createdTaskIdList = userDao.editUserById(user);
		
		//fetch all tasks with owners
		if(createdTaskIdList!=null && createdTaskIdList.size() > 0){
			
			List<Task> allPublishedTasks = taskDao.getAllTasks(false);
			for(Integer taskId : createdTaskIdList){
				for(Task task : allPublishedTasks){
					if(taskId == task.getId()){
						createdTaskList.add(task);
						break;
					}
				}
			}

			//shoot mail to task-owners for new assigned tasks
			if(createdTaskList.size()>0){

				List<EmailModel> messages = new ArrayList<EmailModel>();
				EmailModel msg = null;
				List<String> to = null;
				List<String> cc = null;
				
				Map<String, List<Task>> ownerTaskMap = new HashMap<String, List<Task>>();
				List<Task> ownerTasks = null;
				
				//merge mails directed to same owner
				for(Task task : createdTaskList){
					ownerTasks = ownerTaskMap.get(task.getOwner().getEmail());
					if(ownerTasks == null){
						ownerTasks = new ArrayList<Task>();
						ownerTasks.add(task);
					}else{
						ownerTasks.add(task);
					}
					ownerTaskMap.put(task.getOwner().getEmail(), ownerTasks);
				}
				
				for(String ownerEmail : ownerTaskMap.keySet()){
					if(!StringUtils.isEmpty(ownerEmail)){
						to = new ArrayList<String>();
						to.add(ownerEmail);
					}
					msg = new EmailModel(null, to, cc, CommonConstants.EMAIL_SUBJECT_NEW_ASSIGNED_TASK, Utils.getEmailBodyForNewTask(user, ownerTaskMap.get(ownerEmail), false));
					messages.add(msg);
				}
				
				emailService.sendMail(messages);

			}
		}
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("--------User details updated: "+user);
			if(user.getUserRoles()!=null && user.getUserRoles().size()>0){
				LOGGER.debug("---------Updated User Roles------------");
				for(UserRole ur : user.getUserRoles()){
					LOGGER.debug(ur.toString());
				}
			}
			if(user.getTasks()!=null && user.getTasks().size()>0){
				LOGGER.debug("---------Updated Tasks------------");
				for(Entry<User.TaskRole,List<Task>> entry : user.getTasks().entrySet()){
					LOGGER.debug("---------"+entry.getKey()+" Tasks------------");
					for(Task task : entry.getValue()){
						LOGGER.debug(task.toString());
					}
				}
			}
			LOGGER.debug("-------------------------------------------");
		}
		
		return user;
	}

	@Override
	public User createNewUser(User user) throws UserPortalSystemException {
		
		List<Task> createdTaskList = new ArrayList<Task>();
		
		userDao.validateUserRoles(user);
		List<Integer> createdTaskIdList =  userDao.createNewUser(user);
		
		//fetch all tasks with owners
		List<Task> allPublishedTasks = taskDao.getAllTasks(false);
		for(Integer taskId : createdTaskIdList){
			for(Task task : allPublishedTasks){
				if(taskId == task.getId()){
					createdTaskList.add(task);
					break;
				}
			}
		}

		//shoot mail to task-owners for new assigned tasks
		if(createdTaskList.size()>0){

			List<EmailModel> messages = new ArrayList<EmailModel>();
			EmailModel msg = null;
			List<String> to = null;
			List<String> cc = null;
			
			Map<String, List<Task>> ownerTaskMap = new HashMap<String, List<Task>>();
			List<Task> ownerTasks = null;
			
			//merge mails directed to same owner
			for(Task task : createdTaskList){
				ownerTasks = ownerTaskMap.get(task.getOwner().getEmail());
				if(ownerTasks == null){
					ownerTasks = new ArrayList<Task>();
					ownerTasks.add(task);
				}else{
					ownerTasks.add(task);
				}
				ownerTaskMap.put(task.getOwner().getEmail(), ownerTasks);
			}
			
			for(String ownerEmail : ownerTaskMap.keySet()){
				if(!StringUtils.isEmpty(ownerEmail)){
					to = new ArrayList<String>();
					to.add(ownerEmail);
				}
				msg = new EmailModel(null, to, cc, CommonConstants.EMAIL_SUBJECT_NEW_ASSIGNED_TASK, Utils.getEmailBodyForNewTask(user, ownerTaskMap.get(ownerEmail), false));
				messages.add(msg);
			}
			
			emailService.sendMail(messages);
			
		}
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("--------Created User details: "+user);
			if(user.getUserRoles()!=null && user.getUserRoles().size()>0){
				LOGGER.debug("---------Created User Roles------------");
				for(UserRole ur : user.getUserRoles()){
					LOGGER.debug(ur.toString());
				}
			}
			if(user.getTasks()!=null && user.getTasks().size()>0){
				LOGGER.debug("---------Created Tasks------------");
				for(Entry<User.TaskRole,List<Task>> entry : user.getTasks().entrySet()){
					LOGGER.debug("---------"+entry.getKey()+" Tasks------------");
					for(Task task : entry.getValue()){
						LOGGER.debug(task.toString());
					}
				}
			}
			LOGGER.debug("-------------------------------------------");
		}
		
		return user;
	}

	@Override
	public User login(User credentials) throws UserPortalSystemException {
		
		User user = userDao.login(credentials);
		
		//shoot dummy mail 
		/*List<String> to = null;
		if(!StringUtils.isEmpty(user.getEmail())){
			to = new ArrayList<String>();
			to.add(user.getEmail());
		}
		emailService.sendMail(to, null, CommonConstants.EMAIL_SUBJECT_LOGGED_IN_NOTIFICATION, Utils.getEmailBodyForLogin(user));*/
		
		return user;
	}

	@Override
	public User updateCredentials(User user) throws UserPortalSystemException {
		
		userDao.updateCredentials(user);
		
		//shoot mail to the user 
		List<String> to = null;
		if(!StringUtils.isEmpty(user.getEmail())){
			to = new ArrayList<String>();
			to.add(user.getEmail());
		}
		emailService.sendMail(null, to, null, CommonConstants.EMAIL_SUBJECT_UPDATE_CREDENTIALS, Utils.getEmailBodyForUpdateCredentials(user));

		return user;
	}

	@Override
	public void deleteUserById(User user) throws UserPortalSystemException {
		
		//Confirm no pending tasks to perform
		validateOwnerTasksStatus(user);
		if(user.getTasks()!=null){
			//do not delete user if owns any task
			return;
		}
		
		//fetch all tasks with owners
		List<Task> allPublishedTasks = taskDao.getAllTasks(false);
		
		//delete user and apply off-boarding tasks
		List<Task> reverseTaskList = userDao.deleteUserById(user, allPublishedTasks);
		
		//shoot mail to task-owners for new assigned tasks
		if(reverseTaskList.size()>0){
			
			List<EmailModel> messages = new ArrayList<EmailModel>();
			EmailModel msg = null;
			List<String> to = null;
			List<String> cc = null;
			
			Map<String, List<Task>> ownerTaskMap = new HashMap<String, List<Task>>();
			List<Task> ownerTasks = null;
			
			//merge mails directed to same owner
			for(Task task : reverseTaskList){
				ownerTasks = ownerTaskMap.get(task.getOwner().getEmail());
				if(ownerTasks == null){
					ownerTasks = new ArrayList<Task>();
					ownerTasks.add(task);
				}else{
					ownerTasks.add(task);
				}
				ownerTaskMap.put(task.getOwner().getEmail(), ownerTasks);
			}
			
			for(String ownerEmail : ownerTaskMap.keySet()){
				if(!StringUtils.isEmpty(ownerEmail)){
					to = new ArrayList<String>();
					to.add(ownerEmail);
				}
				msg = new EmailModel(null, to, cc, CommonConstants.EMAIL_SUBJECT_NEW_ASSIGNED_TASK, Utils.getEmailBodyForNewTask(user, ownerTaskMap.get(ownerEmail), true));
				messages.add(msg);
			}
			
			emailService.sendMail(messages);
			
		}
		
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("User successfully Deleted: "+user);
			if(user.getUserRoles()!=null && user.getUserRoles().size()>0){
				LOGGER.debug("---------User Roles After Delete------------");
				for(UserRole ur : user.getUserRoles()){
					LOGGER.debug(ur.toString());
				}
			}
			if(user.getTasks()!=null && user.getTasks().size()>0){
				LOGGER.debug("---------Tasks After Delete------------");
				for(Entry<User.TaskRole,List<Task>> entry : user.getTasks().entrySet()){
					LOGGER.debug("---------"+entry.getKey()+" Tasks------------");
					for(Task task : entry.getValue()){
						LOGGER.debug(task.toString());
					}
				}
			}
			LOGGER.debug("-------------------------------------------");
		}
		
	}

	private void validateOwnerTasksStatus(User user) throws UserPortalSystemException {
		Map<TaskRole, List<Task>> tasks = new HashMap<TaskRole, List<Task>>();
		
		List<Task> ownerTasks = taskDao.getOwnerTasksByUserId(user.getId());
		
		//Filter status for owner tasks
		if(ownerTasks.size()>0){
			
			//Get All Task Status
			List<TaskStatus> allStatus = taskDao.getAllTaskStatus();
			
			for(Task task : ownerTasks){
				List<TaskStatus> statusList = new ArrayList<TaskStatus>();
				for(TaskStatus status : allStatus){
					if(task.getId()==status.getTaskId() && status.getStatusCode()<2) statusList.add(status); //refine NonStarted and Open Tasks
				}
				if(statusList.size()>0){
					task.setAllTaskStatus(statusList);
					user.setIncompletetasks(true); //Set flag - user has incomplete tasks to perform
				}
			}

			tasks.put(TaskRole.Owner, ownerTasks);
			user.setTasks(tasks);
		}
	}

	@Override
	public void forgotCredentials(User user) throws UserPortalSystemException {
		
		User credentials = userDao.forgotCredentials(user);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("forgotCredentials(): User recovered as: "+credentials.getName());
		}
		//shoot mail with password 
		List<String> to = null;
		if(!StringUtils.isEmpty(credentials.getEmail())){
			to = new ArrayList<String>();
			to.add(credentials.getEmail());
		}
		emailService.sendMail(null, to, null, CommonConstants.EMAIL_SUBJECT_FORGOT_PASSWORD, Utils.getEmailBodyForForgotCredentials(credentials));
				
	}
	

}

