package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.dao.ModuleDao;
import org.cts.userportal.model.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Contains business logic related to Modules
 * @author Dhiman Mondal
 *
 */
@Service("moduleService")
public class ModuleServiceImpl implements ModuleService {

	@Autowired
	ModuleDao moduleDao;

	@Override
	public List<Module> getAllModules() {
		return moduleDao.getAllModules();
	}

	@Override
	public Module getModuleById(int id) {
		return moduleDao.getModuleById(id);
	}

	@Override
	public int deleteModuleById(int id, int adminId) {
		return moduleDao.deleteModuleById(id, adminId);	
	}

	@Override
	public Module editModuleById(int id, String name) {
		return moduleDao.editModuleById(id,name);
	}

	@Override
	public String createNewModuleById(String name) {
		return moduleDao.createNewModuleById(name);
	}
	

}




