package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.model.Module;

public interface ModuleService {

	List<Module> getAllModules();

	Module getModuleById(int id);

	Module editModuleById(int id, String name);

	String createNewModuleById(String id);

	int deleteModuleById(int id, int adminId);

}
 