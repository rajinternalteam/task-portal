package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.model.User;
import org.cts.userportal.utility.UserPortalSystemException;

public interface UserService {

	List<User> getAllUsers(boolean excludeUserRoles, boolean excludeTasks, boolean deletedOnly) throws UserPortalSystemException;

	User getUserById(int id, boolean excludeUserRoles, boolean excludeTasks) throws UserPortalSystemException;

	User editUserById(User user) throws UserPortalSystemException;

	User createNewUser(User user) throws UserPortalSystemException;

	User login(User credentials) throws UserPortalSystemException;

	User updateCredentials(User user) throws UserPortalSystemException;

	void deleteUserById(User user) throws UserPortalSystemException;

	void forgotCredentials(User user) throws UserPortalSystemException;

}

