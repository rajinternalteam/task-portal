package org.cts.userportal.service;

import java.util.List;

import org.cts.userportal.dao.GroupDao;
import org.cts.userportal.model.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Contains business logic related to Groups
 * @author Dhiman Mondal
 *
 */

@Service("groupService")
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao groupDao;	
	
	@Override
	public List<Group> getAllGroups() {
		return groupDao.getAllGroups();
	}

	@Override
	public Group getGroupById(int id) {
		return groupDao.getGroupById(id);
	} 

	@Override
	public int deleteGroupById(int id, int adminId) {
		return groupDao.deleteGroupById(id, adminId);
		
	}

	@Override
	public Group editGroupById(int id,String name) {
		return groupDao.editGroupById(id,name);
		
	}

	@Override
	public String createNewGroupById(String name) {
		return groupDao.createNewGroupById(name);
	}

}
