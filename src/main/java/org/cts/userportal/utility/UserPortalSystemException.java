package org.cts.userportal.utility;

@SuppressWarnings("serial")
public class UserPortalSystemException extends Exception{

	public UserPortalSystemException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UserPortalSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UserPortalSystemException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UserPortalSystemException(Throwable cause) {
		super(cause);
	}
	
	

}
