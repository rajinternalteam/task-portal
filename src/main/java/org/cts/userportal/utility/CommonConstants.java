package org.cts.userportal.utility;

/**
 * Contains common constants, SQL queries etc.
 * @author Dhiman Mondal
 *
 */
public interface CommonConstants {

	//SQL - groups
	String SELECT_ALL_FROM_GROUPS = "SELECT * FROM groups";
	String SELECT_GROUP_BY_ID = "SELECT * FROM groups WHERE id=:id";
	String INSERT_ALL_INTO_GROUPS = "INSERT INTO groups (name) VALUES(:name)";
	String DELETE_GROUP_BY_ID = "DELETE FROM groups  WHERE id=:id";
	String UPDATE_GROUP_BY_ID = "UPDATE groups SET name=:name WHERE id=:id";

	//SQL - modules
	String SELECT_ALL_FROM_MODULES = "SELECT * FROM modules";
	String SELECT_MODULE_BY_ID = "SELECT * FROM modules WHERE id=:id";
	String INSERT_ALL_INTO_MODULES = "INSERT INTO modules (name) VALUES(:name)";
	String DELETE_MODULE_BY_ID = "DELETE FROM modules  WHERE id=:id";
	String UPDATE_MODULE_BY_ID = "UPDATE modules SET name=:name WHERE id=:id";

	//SQL - roles
	String SELECT_ALL_FROM_ROLES = "SELECT * FROM roles";
	String SELECT_ROLE_BY_ID = "SELECT * FROM roles WHERE id=:id";
	String INSERT_ALL_INTO_ROLES = "INSERT INTO roles (name, access_index) VALUES(:name, :access_index)";
	String DELETE_ROLE_BY_ID = "DELETE FROM roles  WHERE id=:id";
	String UPDATE_ROLE_BY_ID = "UPDATE roles SET name=:name, access_index=:access_index WHERE id=:id";

	//SQL - user_roles
	String SELECT_ALL_FROM_USER_ROLES = "SELECT ur.id AS id, r.id AS role_id, r.name AS role_name, r.access_index AS access_index,"
			+" g.id AS group_id, g.name AS group_name, m.id AS module_id, m.name AS module_name, ur.user_id"
			+" FROM user_roles ur LEFT OUTER JOIN roles r ON ur.role_id=r.id LEFT OUTER JOIN groups g ON ur.group_id=g.id"
			+" LEFT OUTER JOIN modules m ON ur.module_id=m.id ORDER BY ur.id";
	String SELECT_USER_ROLES_BY_USER_ID = "SELECT ur.id AS id, r.id AS role_id, r.name AS role_name, r.access_index AS access_index,"
			+" g.id AS group_id, g.name AS group_name, m.id AS module_id, m.name AS module_name, ur.user_id"
			+" FROM user_roles ur LEFT OUTER JOIN roles r ON ur.role_id=r.id LEFT OUTER JOIN groups g ON ur.group_id=g.id"
			+" LEFT OUTER JOIN modules m ON ur.module_id=m.id WHERE ur.user_id=:user_id ORDER BY ur.id";
	String SELECT_USER_ROLE_BY_ID = "SELECT * FROM user_roles WHERE id=:id";
	String INSERT_ALL_INTO_USER_ROLES = "INSERT INTO user_roles (role_id, group_id, module_id, user_id) VALUES(:role_id, :group_id, :module_id, :user_id)";
	String DELETE_USER_ROLE_BY_ID = "DELETE FROM user_roles  WHERE id=:id";
	String DELETE_USER_ROLE_BY_USER_ID = "DELETE FROM user_roles  WHERE user_id=:user_id";
	String UPDATE_USER_ROLE_BY_ID = "UPDATE user_roles SET role_id=:role_id, group_id=:group_id, module_id=:module_id, user_id=:user_id WHERE id=:id";
	String SELECT_DISTINCT_USERID_FROM_USER_ROLE_BY_MODULE_ID_AND_GROUP_ID = "SELECT DISTINCT user_id FROM user_roles"
			+" WHERE ((group_id=:group_id and module_id IS NULL) OR (group_id=:group_id and module_id IN (:module_ids))) AND role_id!=1"; //exclude PM from assigning any tasks
	String SELECT_PROJECT_MANAGER_USER_ROLE = "SELECT user_id FROM user_roles WHERE role_id = 1";
	String SELECT_TEAM_LEAD_USER_ROLE = "SELECT user_id FROM user_roles WHERE role_id = 2 AND group_id=:group_id";
	String SELECT_MODULE_LEAD_USER_ROLE = "SELECT user_id FROM user_roles WHERE role_id = 3 AND group_id=:group_id AND module_id=:module_id";
	
	//SQL - users
	String SELECT_ALL_FROM_USERS = "SELECT * FROM users WHERE is_deleted=0";
	String SELECT_DELETED_FROM_USERS = "SELECT * FROM users WHERE is_deleted=1";
	String SELECT_ALL_FROM_USERS_WITH_ROLES = "SELECT u.id, u.name, u.email, u.access_code FROM users u"
			+" LEFT OUTER JOIN ("+SELECT_ALL_FROM_USER_ROLES+") urs ON u.id = urs.user_id WHERE u.is_deleted=0 ORDER BY u.id";
	String SELECT_USER_BY_ID = "SELECT * FROM users WHERE id=:id";
	String INSERT_ALL_INTO_USERS = "INSERT INTO users (name, email, access_code, admin_id) VALUES(:name, :email, :access_code, :admin_id)";
	//String DELETE_USER_BY_ID = "DELETE FROM users  WHERE id=:id";
	String DELETE_USER_BY_ID = "UPDATE users SET is_deleted=1, admin_id=:admin_id, delete_ts=now() WHERE id=:id";
	String UPDATE_USER_BY_ID = "UPDATE users SET name=:name, email=:email, access_code=:access_code, admin_id=:admin_id WHERE id=:id";
	String REBOARD_USER_BY_ID = "UPDATE users SET name=:name, email=:email, access_code=:access_code, admin_id=:admin_id, is_deleted=0 WHERE id=:id";
	String SELECT_USER_BY_CREDENTIALS = "SELECT * FROM users WHERE email=:email AND password=:password";
	String UPDATE_USER_CREDENTIALS = "UPDATE users SET email=:email, password=:password WHERE id=:id";
	String UPDATE_USER_CREDENTIALS_EMAIL_ONLY = "UPDATE users SET email=:email WHERE id=:id";
	String FORGOT_USER_CREDENTIALS = "SELECT * FROM users WHERE email=:email";
	
	//SQL - tasks
	String SELECT_ALL_FROM_TASKS = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, t.owner_id, o.name AS owner_name, o.email AS owner_email,"
			+" t.publisher_id, p.name AS publisher_name, p.email AS publisher_email, t.is_deleted"
			+" FROM tasks t LEFT OUTER JOIN users o ON t.owner_id = o.id"
			+" LEFT OUTER JOIN users p ON t.publisher_id = p.id WHERE t.is_deleted=0";
	String SELECT_DELETED_FROM_TASKS = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, t.owner_id, o.name AS owner_name, o.email AS owner_email,"
			+" t.publisher_id, p.name AS publisher_name, p.email AS publisher_email, t.is_deleted"
			+" FROM tasks t LEFT OUTER JOIN users o ON t.owner_id = o.id"
			+" LEFT OUTER JOIN users p ON t.publisher_id = p.id WHERE t.is_deleted=1";
	String SELECT_TASK_BY_ID = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, t.owner_id, o.name AS owner_name, o.email AS owner_email,"
			+" t.publisher_id, p.name AS publisher_name, p.email AS publisher_email, t.is_deleted"
			+" FROM tasks t LEFT OUTER JOIN users o ON t.owner_id = o.id"
			+" LEFT OUTER JOIN users p ON t.publisher_id = p.id WHERE t.id=:id";
	String SELECT_ALL_TASK_OWNERS = "SELECT t.owner_id, u.name AS owner_name, u.email AS owner_email"
			+" FROM tasks t LEFT OUTER JOIN users u ON t.owner_id = u.id WHERE t.is_deleted=0 AND u.is_deleted=0";
	String SELECT_ALL_TASK_PUBLISHERS = "SELECT t.publisher_id, u.name AS publisher_name, u.email AS publisher_email"
			+" FROM tasks t LEFT OUTER JOIN users u ON t.publisher_id = u.id WHERE t.is_deleted=0 AND u.is_deleted=0";
	String INSERT_INTO_TASKS = "INSERT INTO tasks (description, reverse_task, reverse_id, owner_id, publisher_id, days_to_complete, admin_id)"
			+" VALUES(:description, :reverse_task, :reverse_id, :owner_id, :publisher_id, :days_to_complete, :admin_id)";
	String UPDATE_TASK_BY_ID = "UPDATE tasks SET description=:description, reverse_task=:reverse_task, reverse_id=:reverse_id, owner_id=:owner_id, publisher_id=:publisher_id,"
			+" days_to_complete=:days_to_complete, admin_id=:admin_id WHERE id=:id";
	String REPUBLISH_TASK_BY_ID = "UPDATE tasks SET description=:description, reverse_task=:reverse_task, reverse_id=:reverse_id, owner_id=:owner_id, publisher_id=:publisher_id,"
			+" days_to_complete=:days_to_complete, admin_id=:admin_id, is_deleted=0 WHERE id=:id";
	String DELETE_REVERSE_TASK_BY_ID = "DELETE FROM tasks WHERE id=:id AND reverse_id=-1";
	String DELETE_TASK_BY_ID = "UPDATE tasks SET is_deleted=1, admin_id=:admin_id, delete_ts=now() WHERE id=:id";
	
	//SQL - task_mappings
	String SELECT_ALL_TASK_MAPPINGS = "SELECT tm.id, tm.group_id, g.name AS group_name, tm.module_id, m.name AS module_name, tm.task_id"
			+" FROM task_mappings tm LEFT OUTER JOIN groups g ON tm.group_id=g.id LEFT OUTER JOIN modules m ON tm.module_id=m.id";
	String SELECT_TASK_MAPPINGS_BY_TASK_ID = "SELECT tm.id, tm.group_id, g.name AS group_name, tm.module_id, m.name AS module_name, tm.task_id"
			+" FROM task_mappings tm LEFT OUTER JOIN groups g ON tm.group_id=g.id LEFT OUTER JOIN modules m ON tm.module_id=m.id WHERE tm.task_id=:task_id";
	String SELECT_TASK_ID_BY_GROUP_ID = "SELECT task_id FROM task_mappings WHERE group_id=:group_id AND is_deleted=0";
	String SELECT_TASK_ID_BY_GROUP_ID_AND_MODULE_ID = "SELECT task_id FROM task_mappings WHERE group_id=:group_id AND module_id =:module_id AND is_deleted=0";
	String INSERT_ALL_INTO_TASKS_MAPPING = "INSERT INTO task_mappings (group_id, module_id, task_id) VALUES(:group_id, :module_id, :task_id)";
	String DELETE_TASKS_MAPPING_BY_TASK_ID = "DELETE FROM task_mappings WHERE task_id=:task_id";

	//SQL - task_status
	String SELECT_ALL_TASK_STATUS = "SELECT ts.id, ts.task_id, ts.user_id, u.name AS user_name, u.email AS user_email, u.is_deleted, ts.status, ts.create_ts, ts.update_ts"
			+" FROM task_status ts LEFT OUTER JOIN users u ON ts.user_id = u.id ORDER BY ts.status"; 
			//u.is_deleted removed from condition to allow reverse task flow on user deletion
	String SELECT_TASK_STATUS_BY_TASK_ID = "SELECT ts.id, ts.task_id, ts.user_id, u.name AS user_name, u.email AS user_email, u.is_deleted, ts.status, ts.create_ts, ts.update_ts"
			+" FROM task_status ts LEFT OUTER JOIN users u ON ts.user_id = u.id WHERE ts.task_id=:task_id ORDER BY ts.status"; 
			//u.is_deleted removed from condition to allow reverse task flow on user deletion
	String SELECT_TASK_ID_BY_USER_ID_FROM_TASK_STATUS="SELECT task_id FROM task_status WHERE user_id=:user_id";
	String SELECT_TASK_ID_AND_STATUS_BY_TASK_ID = "SELECT * FROM task_status WHERE task_id=:task_id";
	String SELECT_TASK_STATUS_BY_USER_ID_FROM_TASK_STATUS="SELECT * FROM task_status WHERE user_id=:user_id";
	String INSERT_TASK_STATUS_WITH_TASK_ID_AND_USER_ID ="INSERT INTO task_status (task_id, user_id, admin_id) VALUES(:task_id, :user_id, :admin_id)";
	String UPDATE_TASK_STATUS_BY_USER_ID_AND_TASK_ID ="UPDATE task_status SET status=:status, admin_id=:admin_id WHERE user_id=:user_id AND task_id=:task_id";
	String UPDATE_TASK_STATUS_BY_USER_ID ="UPDATE task_status SET status=:status, admin_id=:admin_id WHERE user_id=:user_id";
	String DELETE_TASK_STATUS_BY_USER_ID ="DELETE FROM task_status WHERE user_id=:user_id";
	String UPDATE_TASK_STATUS_BY_TASK_ID ="UPDATE task_status SET status=:status, admin_id=:admin_id WHERE task_id=:task_id";
	String DELETE_TASK_STATUS_BY_TASK_ID ="DELETE FROM task_status WHERE task_id=:task_id";
	String UPDATE_TASK_STATUS_BY_STATUS_ID ="UPDATE task_status SET status=:status, admin_id=:admin_id WHERE id=:id";
	//String UPDATE_AS_REVERSE_TASK_ID_WITH_STATUS_ID ="UPDATE task_status SET status=0, reverse_task=1, admin_id=:admin_id WHERE id=:id";
	
	//SQL - User Tasks by publisher, owner, user
	String SELECT_ALL_USER_TASKS_BY_USER_ID = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, ts.id AS status_id, ts.status, ts.create_ts, ts.update_ts"
			+" FROM task_status ts LEFT OUTER JOIN tasks t ON ts.task_id = t.id WHERE ts.user_id = :user_id AND t.is_deleted=0 AND ts.status<3 ORDER BY ts.status"; //remove tasks with Closed status from the list
	String SELECT_ALL_OWNER_TASKS_BY_OWNER_ID = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, t.publisher_id, p.name AS publisher_name, p.email AS publisher_email"
			+" FROM tasks t LEFT OUTER JOIN users o ON t.owner_id = o.id LEFT OUTER JOIN users p ON t.publisher_id = p.id"
			+" WHERE t.owner_id = :owner_id AND t.is_deleted=0";
	String SELECT_ALL_PUBLISHER_TASKS_BY_PUBLISHER_ID = "SELECT t.id, t.description, t.reverse_task, t.reverse_id, t.days_to_complete, t.owner_id, o.name AS owner_name, o.email AS owner_email"
			+" FROM tasks t LEFT OUTER JOIN users o ON t.owner_id = o.id LEFT OUTER JOIN users p ON t.publisher_id = p.id"
			+" WHERE t.publisher_id = :publisher_id AND t.is_deleted=0";

	//Error Messages
	String ERROR_MESSAGE_SQL_EXCEPTION = "Database exception occurred";
	String ERROR_MESSAGE_REQUIRED_REQUEST_PARAMETER_UNAVAILABLE = "Required request paramter is unavailable";
	String ERROR_MESSAGE_DUBLICATE_EMAIL ="User with the provided email address already exists";
	String ERROR_MESSAGE_RESTRICTED_DATABASE_OPERATION = "This transaction is restricted in the database";
	String ERROR_MESSAGE_EMAIL_FORMAT_ERROR = "Email address is not acceptable, must be a cognizant email.";
	
	//Constants
	String APPLICATION_DISPLAY_DATE_FORMAT = "dd-MM-yyyy HH:mm";
	int DEFAULT_TASK_TO_COMPLETE_IN_DAYS = 10;
	
	//Email Subjects
	String EMAIL_SUBJECT_NEW_ASSIGNED_TASK = "Resource Portal - New assigned tasks";
	String EMAIL_SUBJECT_FORGOT_PASSWORD = "Resource Portal - Forgot Password?";
	String EMAIL_SUBJECT_LOGGED_IN_NOTIFICATION = "Resource Portal - Logged in Notification";
	String EMAIL_SUBJECT_UPDATE_CREDENTIALS = "Resource Portal - Credentials Update Notification";
	String EMAIL_SUBJECT = "subject";
	String EMAIL_TO = "to";
	String EMAIL_CC = "cc";
	String EMAIL_FROM = "from";
	
	//ERROR CODES
	String ERROR_CODE_USER_DELETE_INCOMPLETE_TASKS = "UD1";
	String ERROR_CODE_USER_DELETE_OWNED_TASKS = "UD2";
	
}
