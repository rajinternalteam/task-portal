// create the module and name it scotchApp
var mainApp = angular.module('mainApp', ['ngRoute','ngCookies','ui.bootstrap']);

// configure our routes
mainApp.config(function($routeProvider) {
	$routeProvider
      
	.when('/login', {
		controller: 'LoginController',
		templateUrl: 'pages/login.html',
		//hideMenus: true
	})

	// route for the home page
	.when('/', {
		templateUrl : 'pages/home.html',
		controller  : 'mainController',
		/*	factory    :'dataShare'*/
		/*	filter   : 'unique'*/
	})

	// route for the about page
	.when('/groups', {
		templateUrl : 'pages/groups.html',
		controller  : 'groupsController'
	})

	// route for the modules page
	.when('/modules', {
		templateUrl : 'pages/modules.html',
		controller  : 'modulesController'
	})

	.when('/tasks', {
		templateUrl : 'pages/tasks.html',
		controller  : 'tasksController' ,
		directive :'dropdownmultiselect'
	})
	.when('/users', {
		templateUrl : 'pages/users.html',
		controller  : 'usersController'
	})

	//route for system and setting page
	.when('/system', {
		templateUrl : 'pages/system.html',
		controller  : 'systemController'
	})  

	// route for the password page
	.when('/password', {
		templateUrl : 'pages/password.html',
		controller  : 'passwordController'
	})

	
	.when('/passwordRetrieve', {
		templateUrl : 'pages/passwordRetrieve.html',
		controller  : 'forgetpasswordController'
	})
});

mainApp.run(['$rootScope', '$location', '$cookieStore', '$http',
             function ($rootScope, $location, $cookieStore, $http) {
	// keep user logged in after page refresh
	$rootScope.globals = $cookieStore.get('globals') || {};
	if ($rootScope.globals.currentUser) {
		$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
	}

	$rootScope.$on('$locationChangeStart', function (event, next, current) {
		// redirect to login page if not logged in
		if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
			if($location.path() == '/passwordRetrieve'){
				$location.path('/passwordRetrieve');
			}else{
				$location.path('/login');
			}
			$('.left-nav li').removeClass('nav-selected');
		}
	});
}]);


function selectLeftNav(){
	var href = window.location.href;
	$('.left-nav li').removeClass('nav-selected');
	if(href.indexOf('/#/system') > 0){
		$('.sys-nav').addClass('nav-selected');
	}else if(href.indexOf('/#/modules') > 0){
		$('.mod-nav').addClass('nav-selected');
	}else if(href.indexOf('/#/users') > 0){
		$('.users-nav').addClass('nav-selected');
	}else if(href.indexOf('/#/password') > 0){
		$('.pass-nav').addClass('nav-selected');
	}else if(href.indexOf('/#/tasks') > 0){
		$('.tasks-nav').addClass('nav-selected');
	}else{
		$('.db-nav').addClass('nav-selected');
	}
}

var ERROR_MESSAGES = {
	'UD1' : 'User has following tasks to perform, All pending Tasks must be completed before De-Registration:',
	'UD2' : 'User performs following tasks, Please change the performer of the task before De-Registration:',
}
