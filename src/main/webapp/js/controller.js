//create the controller and inject Angular's $scope
mainApp.controller('mainController', function($scope, $http, dataShare,$rootScope,$route,$modal) {
	$rootScope.changeemail=false;
	$scope.count=0;
	// create a message to display in our view
	$scope.statuschangemodal= function (id,status,value,parentindex,currentindex) {
		console.log(id,status,value,parentindex,currentindex)

		if(status=="Closed"){
			$modal.open({
				templateUrl: 'pages/statusChange.html',
				controller: 'modalController',
				scope: $scope
			})
			.result.then(function() {
				$scope.statuschange(id,status,value,parentindex,currentindex)
			}, function() {

			})  
		}
		else{
			$scope.statuschange(id,status,value,parentindex,currentindex)
		}
	}

	selectLeftNav();
	$scope.Closed=0;
	$scope.Completed=0;
	$scope.Wip=0;
	$scope.NotStarted=0;
	$scope.Own_Closed=0;
	$scope.Own_Completed=0;
	$scope.Own_Wip=0;
	$scope.Own_NotStarted=0;
	$scope.own_statuses= [
	                      {'id':2,'name':'Completed'},
	                      {'id':3,'name':'Closed'},	
	                      ];
	$scope.statuses= [
	                  {'id':0,'name':'NotStarted'},
	                  {'id':1,'name':'WIP'},
	                  {'id':2,'name':'Completed'},
	                  //  {'id':3,'name':'Closed'},	
	                  ];
	$scope.totalStatuses= [
	                       {'id':0,'name':'NotStarted'},
	                       {'id':1,'name':'WIP'},
	                       {'id':2,'name':'Completed'},
	                       {'id':3,'name':'Closed'},	
	                       ];


	$scope.userid=$rootScope.globals.currentUser.id
	$http.get("/whguserportal/services/users/"+$scope.userid)
	.then(function (response) {$scope.user = response.data;
	console.log($scope.user.tasks.Publisher)
	/*	for(var i=0;i<$scope.user.tasks.Publisher.length;i++){
		for(var j=0;j<$scope.user.tasks.Publisher[i].allTaskStatus.length;j++){

		if($scope.user.tasks.Publisher[i].allTaskStatus[j].status=="NotStarted"){
			$scope.NotStarted=$scope.NotStarted+1;
		}
		else if($scope.user.tasks.Publisher[i].allTaskStatus[j].status=="WIP"){
			$scope.Wip=$scope.Wip+1;
		}
		else if($scope.user.tasks.Publisher[i].allTaskStatus[j].status=="Completed"){
			$scope.Completed=$scope.Completed+1;
		}
		else if($scope.user.tasks.Publisher[i].allTaskStatus[j].status=="Closed"){
			$scope.Closed=$scope.Closed+1;
		}
		}

	}*/

	for(var i=0;i<$scope.user.tasks.Owner.length;i++){
		for(var j=0;j<$scope.user.tasks.Owner[i].allTaskStatus.length;j++){

			if($scope.user.tasks.Owner[i].allTaskStatus[j].status=="NotStarted" && $scope.user.tasks.Owner[i].allTaskStatus[j].user.name!= $scope.user.name){
				$scope.NotStarted=$scope.NotStarted+1;
			}
			else if($scope.user.tasks.Owner[i].allTaskStatus[j].status=="WIP" && $scope.user.tasks.Owner[i].allTaskStatus[j].user.name!= $scope.user.name){
				$scope.Wip=$scope.Wip+1;
			}
			else if($scope.user.tasks.Owner[i].allTaskStatus[j].status=="Completed"  && $scope.user.tasks.Owner[i].allTaskStatus[j].user.name!= $scope.user.name){
				$scope.Completed=$scope.Completed+1;
			}
			else if($scope.user.tasks.Owner[i].allTaskStatus[j].status=="Closed" &&  $scope.user.tasks.Owner[i].allTaskStatus[j].user.name!= $scope.user.name){
				$scope.Closed=$scope.Closed+1;
			}
		}

	}


	for(var i=0;i<$scope.user.tasks.User.length;i++){
		if($scope.user.tasks.User[i].status.status=="NotStarted" ){
			$scope.Own_NotStarted=$scope.Own_NotStarted+1;
		}
		else if($scope.user.tasks.User[i].status.status=="WIP"){
			$scope.Own_Wip=$scope.Own_Wip+1;
		}
		else if($scope.user.tasks.User[i].status.status=="Completed"){
			$scope.Own_Completed=$scope.Own_Completed+1;
		}
		else if($scope.user.tasks.User[i].status.status=="Closed"){
			$scope.Own_Closed=$scope.Own_Closed+1;
		}


	}




	});

	$scope.own_changestatus=[];
	$scope.pub_changestatus=[];
	$scope.user_changestatus=[];
	console.log($scope.user)

	$scope.pub_statuschange=function(value,pi,index){
		console.log($scope.user)
		$scope.pub_changestatus[pi]=[];

		$scope.pub_changestatus[pi][index]=value;
		if (value=='false'){
			// Call service
		}
	}

	$scope.user_statuschange=function(value,pi,index){
		$scope.user_changestatus[pi]=[];
		$scope.user_changestatus[pi][index]=value;
		if (value=='false'){
			// Call service
		}
	}

	$scope.own_statuschange=function(value,pi,index){
		$scope.own_changestatus[pi]=[];
		$scope.own_changestatus[pi][index]=value;
		if (value=='false'){
			// Call service
		}
	}
	$scope.statuschange=function(id,status,value,parentindex,currentindex){
		for(var i=0;i<$scope.totalStatuses.length;i++){
			if($scope.totalStatuses[i].name==status){
				$scope.statusId=$scope.totalStatuses[i].id;
				$scope.user_statuschange(value,parentindex,currentindex)
				break;
			}
		}
		$scope.data= {'id': id ,'statusCode': $scope.statusId}
		console.log($scope.data)
		$scope.urlgenerated="/whguserportal/services/tasks/status/"
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'POST',
				data: {'id': id ,'statusCode': $scope.statusId},
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				console.log('successfully status changed');
				console.log(response)
				$scope.user_statuschange(value,parentindex,currentindex)
				return response;
			}).error(function(errResponse){
				$scope.user_statuschange(value,parentindex,currentindex)
				console.error('Error while changing status');
			});
	}


	$scope.taskedit=function(id, isDeleted){
		/* dataShare.sendData(id); */
		dataShare.dataObj.id=id;
		dataShare.dataObj.change=true;
	}

	$scope.owned_task =false;
	$scope.published_task =false;
	$scope.user_task =true;
	$("#dashboard .user_btn").addClass('nav-selected');
	$scope.task_show=function(tasktype) {
		//$("#dashboard .nav-tabs li").removeClass('nav-selected');
		if(tasktype=="owned"){
			$scope.owned_task = true;
			//$scope.published_task =false;
			$scope.user_task =false;
			$("#dashboard .owned_btn").addClass('nav-selected');
			$("#dashboard .user_btn").removeClass('nav-selected');
		}
		else if(tasktype=="published"){
			if($scope.count==0){
				$http.get("/whguserportal/services/tasks/")
				.then(function (response) {$rootScope.tasks = response.data;});
				$scope.count++;
			}
			//$scope.owned_task =false;
			//$scope.user_task =false;
			$scope.published_task=$scope.published_task=== true ? false: true;;
			//	$("#dashboard .published_btn").addClass('nav-selected');
		}
		else if(tasktype=="user"){
			$scope.owned_task =false;
			//$scope.published_task =false;
			$scope.user_task =true;
			$("#dashboard .user_btn").addClass('nav-selected');
			$("#dashboard .owned_btn").removeClass('nav-selected');
		}
	}
});
mainApp.filter('unique', function() {
	return function(collection, keyname) {
		var output = [], 
		keys = [];

		angular.forEach(collection, function(item) {
			var key = item[keyname];
			if(keys.indexOf(key) === -1) {
				keys.push(key);
				output.push(item);
			}
		});
		return output;
	};
	// In case we want to show roles , group and modules separately un-comment
	// it.
	/*
	 * return function (arr, targetFields) {
	 * 
	 * var values = [],parts=[], i,v, unique, l = arr.length, results = [], obj;
	 * parts =targetFields.split(".", 2); var targetField = parts[0]; var
	 * string2 = parts[1];
	 *  // Iterate over all objects in the array // and collect all unique
	 * values for( i = 0; i < arr.length; i++ ) { obj = arr[i]; // check for
	 * uniqueness if(obj[targetField]!=null){ unique = true; for( v = 0; v <
	 * values.length; v++ ){ console.log(v); if( obj[targetField][string2] ==
	 * values[v][string2] ){ unique = false; } } // If this is indeed unique,
	 * add its // value to our values and push // it onto the returned array if(
	 * unique ){ values.push( obj[targetField] ); results.push( obj ); } } }
	 * return results; };
	 */

});


mainApp.controller('groupsController', function($scope) {
	$scope.message = 'Add Modify Delete Groups.';
});




mainApp.controller('modalController', ['$scope', function($scope) {

}]);

mainApp.controller('modulesController', function($scope, $http,$route,$modal,$rootScope) {
	selectLeftNav();
	$scope.remove = function (id,type) {
		$scope.number=id;
		$scope.type=type;
		$modal.open({
			templateUrl: 'pages/modal.html',
			controller: 'modalController',
			scope: $scope
		})
		.result.then(function() {
			$scope.confirmdelete($scope.number)
		}, function() {

		});

	};

	$scope.clear = function(){
		$route.reload();
		console.log('cleared!');
	};
	
	$http.get("/whguserportal/services/modules/")
	.then(function (response) {$scope.modules = response.data;});
	$scope.toggleEdit_module = function (showEdit) {
		this.editMode_module = showEdit;  	   
	}; 

	$http.get("/whguserportal/services/groups/")
	.then(function (response) {$scope.groups = response.data;});
	$scope.toggleEdit_group = function (showEdit) {
		this.editMode_group = showEdit;
	}; 

	$http.get("/whguserportal/services/roles/")
	.then(function (response) {$scope.roles = response.data;});
	$scope.toggleEdit_roles = function (showEdit) {
		this.editMode_roles = showEdit;
	}; 
	$scope.length= 0;
	$scope.formshow =function(){
		$scope.formshowEnabled = $scope.formshowEnabled=== true ? false: true;
	};
	$scope.confirmdelete=function(number){
		if($scope.type=="modules"){
			$scope.urlgen="/whguserportal/services/modules/delete/"+number
			$scope.removeRow(number,$scope.type) }
		else if($scope.type=="groups"){
			$scope.urlgen="/whguserportal/services/groups/delete/"+number
			$scope.removeRow(number,$scope.type)}
		else if($scope.type=="roles"){
			$scope.urlgen="/whguserportal/services/roles/delete/"+number
			$scope.removeRow(number,$scope.type) 
		}
	}

	$scope.removeRow = function(id,type){
		$http({
			url:  $scope.urlgen,
			dataType: 'text',
			method: 'DELETE',
			data: {'adminId':$rootScope.globals.currentUser.id},
			headers: {
				"Content-Type": "text/html"
			}

		}).success(function(response){
			$scope.response = response;
			$scope.done=true;
			console.log("successfully deleted "+type)
			$route.reload();
		}).error(function(errResponse){
			console.error('Error while deleting '+type);
		});

	}  
	$scope.addRow = function(previous_index){		
		$scope.formshowEnabled=false;
	};
	$scope.modules_modify = function (showEdit_module,id,name) {
		this.editMode_module = showEdit_module;
		$scope.urlgenerated="/whguserportal/services/modules/edit/"
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'PUT',
				data: {'id': id ,'name': name,'adminId':$rootScope.globals.currentUser.id},
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){	
				$route.reload();
				console.log('successfully module edited');
				return response;
			}).error(function(errResponse){
				console.error('Error while editing user');
			});
	}


	// group scripts



	$scope.toggleEdit_group = function (showEdit_group) {
		this.editMode_group = showEdit_group;
	};
	$scope.modify_group = function (showEdit_group,id,name) {
		this.editMode_group = showEdit_group;
		$scope.urlgenerated="/whguserportal/services/groups/edit/"
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'PUT',
				data: {'id': id ,'name': name,'adminId':$rootScope.globals.currentUser.id},
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				$route.reload();	
				console.log('successfully group edited');
				return response = response;
			}).error(function(errResponse){
				console.error('Error while editing user');
			});

	};
	$scope.formshow_group =function(){
		$scope.formshowEnabled_group = $scope.formshowEnabled_group=== true ? false: true;
	}
	// row scripts
	$scope.toggleEdit_role = function (showEdit_role) {
		this.editMode_role = showEdit_role;
	};
	$scope.formshow_role =function(){
		$scope.formshowEnabled_role = $scope.formshowEnabled_role== true ? false: true;
	}



	$scope.modify_role = function (showEdit_role,id,name,accessLevel) {
		this.editMode_role = showEdit_role;
		$scope.urlgenerated="/whguserportal/services/roles/edit/"
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'PUT',
				data: {'id': id ,'name': name,'accessLevel':accessLevel,'adminId':$rootScope.globals.currentUser.id},
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				$route.reload();
				console.log('successfully role edited');
				return response = response;
			}).error(function(errResponse){
				console.error('Error while editing user');
			});
	}

});

mainApp.controller('tasksController', function($scope,$http,dataShare,$rootScope,$route,$modal) {

	selectLeftNav();
	$scope.count=0;
	$scope.selectrole=false;
	$scope.showname="Choose Name"
		$scope.showrole="Choose Role"
			$scope.selectType=function(value,name){
		if(value==false && name!=null){
			$scope.showname=name;
		}

		$scope.selectrole=value;
	}
	
	$scope.clearTask = function(){
		$route.reload();
		console.log('task cleared!');
	};

	$http.get("/whguserportal/services/groups/")
	.then(function (response) {$scope.groups = response.data;});
	$http.get("/whguserportal/services/modules/")
	.then(function (response) {$scope.modules = response.data;});
	$http.get("/whguserportal/services/users/?excludeTasks=true") 
	.then(function (response) {$scope.usernames = response.data;
	$scope.roleschildlist=[];
	console.log($scope.usernames)

	$scope.roleslist=[];
	$scope.roleavailable=[]
	$scope.roleslist=$scope.usernames;
	//console.log($scope.roleslist)
	for(var i=0;i<$scope.roleslist.length;i++){
		for(var j=0;j<$scope.roleslist[i].userRoles.length;j++){

			if($scope.roleslist[i].userRoles[j].name.indexOf("Team Member")==-1 ){
				$scope.roleavailable.push($scope.roleslist[i])	
				break;
			}		
		}
	}
	// console.log($scope.roleavailable)
	for(var i=0;i<$scope.roleavailable.length;i++){
		var k=0;
		for(var j=0;j<$scope.roleavailable[i].userRoles.length;j++){
			//	console.log(j)
			//console.log($scope.roleavailable[i].userRoles[j].name)
			if($scope.roleavailable[i].userRoles[j].name.indexOf("Team Member")!=-1){
				//console.log(k)
				//console.log($scope.roleavailable[i].userRoles)
				$scope.roleavailable[i].userRoles.splice(j,1);
				console.log($scope.roleavailable[i].userRoles)
				k=k+1;
				j--;
				//console.log(k)
			}
			//console.log(j)
		}
	}





	/*	for(var i=0;i<$scope.roleslist.length;i++){

		for(var j=0;j<$scope.usernames[i].userRoles.length;j++){

			//$scope.roleslist.push({"id":$scope.usernames[i].id, "name":$scope.usernames[i].name, "role":$scope.usernames[i].userRoles[j].name})
			$scope.roleslist.push($scope.usernames);
		}


	}*/
	/*for(var i=0;i<$scope.roleslist.length;i++){
		console.log($scope.roleslist[i].role)
		var found=false;
		for(var j=i+1;j<$scope.roleslist.length;j++){
			console.log($scope.roleslist[j].role)
			if($scope.roleslist[i].role==$scope.roleslist[j].role){
				found=true;
			}

			}
		if((!found) &&($scope.roleslist[i].userRoles[i].name.indexOf("Team Member")==-1 )){
			$scope.roleavailable.push($scope.roleslist[i]);
		}*/

	/*if((!found) &&($scope.roleslist[i].role.indexOf("General")==-1 )){
			$scope.roleavailable.push($scope.roleslist[i]);
		}
	}*/
	console.log($scope.roleslist);
	console.log($scope.roleavailable);
	/*	for(var i=0;i<$scope.roleslist.length;i++)
		{ 
		var sh=$scope.roleslist[i]
			if(sh.equals("Team Lead") !=-1){
			console.log("hello")
			$scope.roleschildlist.push($scope.roleslist[i])
		}

	}*/
//	console.log($scope.roleschildlist)
//	console.log($scope.roleslist)
	});


	$scope.roleToShow
	$scope.taskresult=false;
	$scope.success=false;

	$scope.days=[];
	for(var i=1;i<=10;i++){
		$scope.days.push(i);

	}





	/*console.log($scope.roleslist)
	$scope.hope=function(){
	for(var i=0;i<$scope.roleslist.length;i++){
		{
			if($scope.roleslist[i].contains('Team Lead') || $scope.roleslist[i].contains('Module Lead') || $scope.roleslist[i].contains('Project Manager'))
			console.log("hello")
			$scope.roleschildlist.push($scope.roleslist[i])
		}

	}
	console.log("hello6666")
	}*/



	$scope.opt="Choose Group"
		$scope.member = {roles: []};
	$scope.selected_items = [];   
	$scope.change=false;
	$scope.formshowEnabled_tasks=false;
	$scope.formshowEnabled_deletedTasks=false;
	$scope.id= dataShare.dataObj.id;
	$scope.change=dataShare.dataObj.change;

	dataShare.dataObj.change=false;
	$scope.mod=dataShare.dataObj.task;
	$rootScope.model=[];
	$scope.count=0;
	$scope.delCallcount=0;

	$scope.taskedit=function(id, isDeleted){
		console.log('isDeleted? '+isDeleted);
		$scope.id=id;
		$scope.formshowEnabled_tasks = !isDeleted;
		$scope.formshowEnabled_deletedTasks = isDeleted;
		$scope.change=true
		$scope.url='/whguserportal/services/tasks/'+$scope.id;
		$rootScope.model=[];

		$http.get($scope.url)
		.then(function (response) {
			$scope.task = response.data;
			if($scope.task.mappings && $scope.task.mappings.length>0){
				for(var i=0; i<$scope.task.mappings.length; i++){
					$scope.count=$scope.count+1;
					$rootScope.model.push($scope.task.mappings[i].moduleId);
				}   
				$scope.opt=$scope.task.mappings[0].groupName;
				$scope.task.mappings[0].id=$scope.task.mappings[0].groupId;
				$scope.task.mappings[0].name=$scope.task.mappings[0].groupName;

				dataShare.dataObj.m1=$rootScope.model;
			}else{
				//for deleted tasks
				$scope.count=$scope.count+1;
				//$rootScope.model.push($scope.task.mappings[i].moduleId);
				$scope.opt=$scope.groups;
				$scope.task.mappings = [{}];
				$scope.task.mappings[0].id=$scope.groups[0].groupId;
				$scope.task.mappings[0].name=$scope.groups[0].groupName;
				dataShare.dataObj.m1={};
			}
			
			$scope.$parent.editTask=true;
			if(isDeleted)
			{
				$scope.task.deleted = true;
				$scope.$parent.republishTask=true;
			}else{
				$scope.$parent.republishTask=false;	
			}
		});
		dataShare.dataObj.tasks=$scope.task;
		$scope.mod=dataShare.dataObj.tasks;

	}
	// $scope.fieldfill=function(){
	if($scope.change==true){

		$scope.url='/whguserportal/services/tasks/'+$scope.id;
		$http.get($scope.url)
		.then(function (response) {$scope.task = response.data;
		for(var i=0; i<$scope.task.mappings.length; i++){
			$scope.count=$scope.count+1;

			$rootScope.model.push($scope.task.mappings[i].moduleId); }
		$scope.opt=$scope.task.mappings[0].groupName;
		$scope.task.mappings[0].id=$scope.task.mappings[0].groupId;

		$scope.task.mappings[0].name=$scope.task.mappings[0].groupName;
		dataShare.dataObj.m1=$scope.model;


		});
		dataShare.dataObj.tasks=$scope.task;
		$scope.mod=dataShare.dataObj.tasks;
	}



	$scope.groupopt=function(optd){

		console.log(optd)
		if(optd!=null){

			$scope.opt=optd;

		}
		else{
			$scope.opt="Choose Role";


		}
	}

	$scope.owneropt=function(optt){

		if(optt!=null){

			$scope.opto=optt;

		}
		else{


		}
	}
	$scope.daysopt=function(optt){

		if(optt!=null){

			$scope.opto=optt;

		}
		else{


		}
	}

	console.log("count"+$scope.count)
	console.log("tasks"+$rootScope.tasks)

	$scope.formshow_tasks=function(){
		console.log("check")
		console.log("count"+$scope.count)
		$scope.formshowEnabled_tasks = $scope.formshowEnabled_tasks== true ? false: true;
		$scope.formshowEnabled_deletedTasks = false;
		if($scope.count==0 ){
			console.log($scope.count)
			$http.get("/whguserportal/services/tasks/?excludeStatus=true")
			.then(function (response) {$rootScope.tasks = response.data;});
			$scope.count++;
		}

		/*$http.get("/whguserportal/services/users/"+$rootScope.globals.currentUser.id)
		.then(function (response) {$scope.user = response.data;});*/
	}  
	$scope.noselected=false;
	$scope.TaskModification=function(task){
		console.log($scope.task.owner)
		console.log('isRepublished? = '+task.deleted);
		task.rePublished = task.deleted;
		$scope.task=task;		

		$scope.noselected=false;
		if($rootScope.model.length==0){
			$scope.noselected=true;

		}
		if($scope.noselected==false){
			$scope.taskresult=true;
			$scope.groupid=$scope.task.mappings[0].id;
			$scope.groupName=$scope.task.mappings[0].name;

			$scope.task.mappings=[]
			$scope.module=[]

			for(var i=0;i<$rootScope.model.length;i++){
				for(var j=0;j<$scope.modules.length;j++){
					if($rootScope.model[i]==$scope.modules[j].id)
						$scope.module.push({"moduleId":$scope.modules[j].id,"moduleName":$scope.modules[j].name})
				}
			}

			for(var i=0;i<$scope.module.length;i++){

				$scope.task.mappings.push({"groupId":$scope.groupid,"groupName":$scope.groupName,"moduleId":$scope.module[i].moduleId,"moduleName":$scope.module[i].moduleName})
				//console.log($scope.task.mappings)			  
			}

			if ($scope.change==true){
				$scope.newtaskresult=false;
				$scope.urlgenerated="/whguserportal/services/tasks/add/?modify=true"
					console.log("edit")
					$scope.data={
					"reverseId":$scope.task.reverseId,
					"adminId":$rootScope.globals.currentUser.id,
					"id": $scope.task.id,
					"description": $scope.task.description,
					"revDescription":$scope.task.revDescription,
					"publisher": {
						"name":$rootScope.globals.currentUser.name,
						"id": $rootScope.globals.currentUser.id,
						"email":$rootScope.globals.currentUser.username
					},
					"owner": {
						"id": $scope.task.owner.id,
						"name": $scope.task.owner.name,
						"email":$scope.task.owner.email
					},
					"mappings": $scope.task.mappings,
					"daysToComplete": $scope.task.daysToComplete,
					"rePublished": $scope.task.rePublished
				}

			}else{
				$scope.newtaskresult=true;
				$scope.urlgenerated="/whguserportal/services/tasks/add/"
					console.log("new")
					$scope.data={
					'adminId':$rootScope.globals.currentUser.id,
					"description": $scope.task.description,
					"revDescription":$scope.task.revDescription,
					"publisher": {
						"id": $rootScope.globals.currentUser.id,
						"name":$rootScope.globals.currentUser.name,
						"email":$rootScope.globals.currentUser.username
					},
					"owner": {
						"id": $scope.task.owner.id,
						"name": $scope.task.owner.name,
						"email":$scope.task.owner.email
					},
					"mappings": $scope.task.mappings,
					"daysToComplete": $scope.task.daysToComplete
				}
			}
			console.log($scope.data)
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'PUT',
				data: $scope.data,
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				$scope.taskresult=true;
				console.log("successfully task added  ");
				this.response = response;

				$scope.addedtask=response;
				$scope.success=task;


				$scope.success3=true;
				return response;


			}).error(function(errResponse){
				console.error('Error while editing tasks');
			})

		}




		$scope.taskhome=function(){
			$scope.taskresult=false;
			$route.reload();

		}

	}
	$scope.removetask = function (id, reverseId) {
		$modal.open({
			templateUrl: 'pages/modal.html',
			controller: 'modalController',
			scope: $scope
		})
		.result.then(function() {
			$scope.confirmdelete(id, reverseId)
		}, function() {

		});

	};
	$scope.confirmdelete = function(id, reverseId){
		$scope.urlgen="/whguserportal/services/tasks/delete/"
			$http({
				url:  $scope.urlgen,
				dataType: 'text',
				method: 'DELETE',
				data: {'id': id,'adminId':$rootScope.globals.currentUser.id, 'reverseId':reverseId},
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				$scope.response = response;
				$scope.done=true;
				console.log("successfully deleted task")
				$route.reload();
			}).error(function(errResponse){
				console.error('Error while deleting task');
			});

	};

	$scope.formshow_deletedTasks=function(){
		console.log("check")
		console.log("delCallcount"+$scope.delCallcount)
		$scope.formshowEnabled_tasks = false;
		$scope.formshowEnabled_deletedTasks = $scope.formshowEnabled_deletedTasks== true ? false: true;
		if($scope.delCallcount==0 ){
			console.log($scope.delCallcount)
			$http.get("/whguserportal/services/tasks/?excludeMappings=true&excludeStatus=true&deletedOnly=true")
			.then(function (response) {$rootScope.deletedTasks = response.data;});
			$scope.delCallcount++;
		}

		/*$http.get("/whguserportal/services/users/"+$rootScope.globals.currentUser.id)
				.then(function (response) {$scope.user = response.data;});*/
	}
	$scope.pre_selected='';

	$scope.openDropdown = function(){      
		/*
		 * if (_.contains($rootScope.model, id)) { console.log("ye challa"+id)
		 * console.log($rootScope.model) return 'fa fa-check'; }
		 */
		/*
		 * $scope.selected_items = []; console.log($scope.pre_selected);
		 * console.log($scope.pre_selected.length) for(var i=0; i<$scope.pre_selected.length;
		 * i++){
		 * 
		 * 
		 * $scope.selected_items.push($scope.pre_selected[i].moduleName);
		 * console.log($scope.selected_items);
		 * $rootScope.model.push($scope.pre_selected[i].moduleId);
		 * console.log($rootScope.model); }
		 */ };

		 $scope.count=0;
		 // $scope.model=dataShare.dataObj.m1;

		 $scope.openDropdown = function(){ 

			 // $scope.selected_items = [];
			 $scope.copieddata=$scope.pre_selected;



			 if($scope.count==0){
				 for(var i=0; i<$scope.pre_selected.length; i++){
					 $scope.count=$scope.count+1;

					 $rootScope.model.push($scope.pre_selected[i].moduleId);  
					 /*
					  * $scope.pre_selected.moduleId =
					  * _.without($scope.pre_selected.moduleId,$scope.copieddata[i].moduleId);
					  * $scope.copieddata =
					  * _.without($scope.model,$scope.copieddata[i].moduleId);
					  */ 	
				 }}
		 }

		 $scope.selectAll = function () {
			 $rootScope.model = _.pluck($scope.modules, 'id');

		 };            
		 $scope.deselectAll = function() {
			 $rootScope.model=[];

		 };
		 $scope.setSelectedItem = function(){
			 var id = this.option.id;
			 if (_.contains($rootScope.model, id)) {
				 $rootScope.model = _.without($rootScope.model, id);      	
			 } else {

				 $rootScope.model.push(id);
			 }

			 return false;
		 };
		 $scope.isChecked = function (id) {                 
			 if (_.contains($rootScope.model, id)) {

				 return 'fa fa-check';
			 }
			 return false;
		 };  
		 
		 
		 $scope.filterTasks = function(task){
			 console.log('filterTasks --> ');
			 console.log('group --> '+($scope.filterbygroup!=undefined?$scope.filterbygroup.id:''));
			 console.log('module --> '+($scope.filterbymodule!=undefined?$scope.filterbymodule.id:''));
			 console.log('owner --> '+($scope.filterbyowner!=undefined?$scope.filterbyowner.id:''));
			 var matched = false;
			 if(task!=undefined && ($scope.filterbygroup!=undefined || $scope.filterbymodule!=undefined || $scope.filterbyowner!=undefined)){
				 if($scope.filterbygroup!=undefined || $scope.filterbymodule!=undefined){
					 for(var i=0; i<task.mappings.length; i++){
						 if($scope.filterbygroup!=undefined && $scope.filterbymodule!=undefined){
							 if($scope.filterbygroup.id==task.mappings[i].groupId && $scope.filterbymodule.id==task.mappings[i].moduleId){
								 matched = true;
								 break;
							 }
						 }else if($scope.filterbygroup!=undefined){
							 if($scope.filterbygroup.id==task.mappings[i].groupId){
								 matched = true;
								 break;
							 }
						 }else if($scope.filterbymodule!=undefined){
							 if($scope.filterbymodule.id==task.mappings[i].moduleId){
								 matched = true;
								 break;
							 }
						 }
					 }
				 }
				 
				 if($scope.filterbyowner!=undefined){
					 if($scope.filterbygroup!=undefined || $scope.filterbymodule!=undefined){
						 if(matched && task.owner.email == $scope.filterbyowner.email){
							 matched = true;
						 }else{
							 matched = false;
						 }
					 }else if(task.owner.email == $scope.filterbyowner.email){
						 matched = true;
					 }
				 }
				 
			 }else{
				 matched = true;
			 }
			 console.log('matched --> '+matched);
			 return matched;
			 
		 }
}

);

/*
mainApp.directive('dropdownmultiselect', function(dataShare,$rootScope){
 return { 
	 restrict: 'E',
	 scope:{ 
		 model: '=',
		 options: '=',
		 pre_selected:'=preSelected'
			 }, 
	template: "<div class='btn-group' data-ng-class='{open:open}'>"+
	"<button class='btn btn-primary dropdown-toggle'>Choose Modules</button>"+ 
	"<button class='btn btn-primary dropdown-toggle ' data-ng-click='open=!open;openDropdown()'><span class='caret'></span></button>"+
	"<ul class='dropdown-menu' aria-labelledby='dropdownMenu'>" + 
	"<li><a ng-click='selectAll()'><i class='fa fa-check-circle'></i> Check All</a></li>" +
	"<li><a ng-click='deselectAll();'><i class='fa fa-times-circle'></i> Uncheck All</a></li>" + "<li class='divider'></li>" +
	"<li data-ng-repeat='option in options'><a data-ng-click='setSelectedItem()'>{{option.name}}<span data-ng-class='isChecked(option.id)'></span></a></li>" +
	"</ul>" +
    "</div>" ,

 controller: function($scope,$rootScope){ $scope.pre_selected='';

$scope.openDropdown = function(){ $scope.selected_items = [];
  console.log($scope.pre_selected); console.log($scope.pre_selected.length)
  for(var i=0; i<$scope.pre_selected.length; i++){


 $scope.selected_items.push($scope.pre_selected[i].moduleName);
  console.log($scope.selected_items);
  $rootScope.model.push($scope.pre_selected[i].moduleId);
  console.log($rootScope.model); } };

  $scope.count=0; // $scope.model=dataShare.dataObj.m1;
  console.log("model444");
  console.log($rootScope.model) 
  $scope.openDropdown =function(){
   // $scope.selected_items = []; $scope.copieddata=$scope.pre_selected;



  if($scope.count==0){ for(var i=0; i<$scope.pre_selected.length; i++){
  $scope.count=$scope.count+1; console.log($scope.count)
  $rootScope.model.push($scope.pre_selected[i].moduleId);
  $scope.pre_selected.moduleId =
  _.without($scope.pre_selected.moduleId,$scope.copieddata[i].moduleId);
  $scope.copieddata = _.without($scope.model,$scope.copieddata[i].moduleId); }} }

  $scope.selectAll = function () {
	  $rootScope.model = _.pluck($scope.options,'id');
	  console.log($rootScope.model);
   };
   $scope.deselectAll = function() {
	   $rootScope.model=[];
  console.log($rootScope.model);
  };
  $scope.setSelectedItem = function(){
	  var id =this.option.id;
	  if (_.contains($rootScope.model, id)) {
		  $rootScope.model =_.without($rootScope.model, id);
		  } else
		  {
  $rootScope.model.push(id);} 
	  console.log($rootScope.model);
	  return false; 
	  };
  $scope.isChecked = function (id) { 
	  if (_.contains($rootScope.model, id)) {
  return 'fa fa-check'; } return false;
  };
  } 
			 }

 });*/









mainApp.controller('usersController', function($scope, $http,$route,$modal,$rootScope) {
	selectLeftNav();
	
	$scope.clearUser = function(){
		$route.reload();
		console.log('user cleared!');
	};
	
	$scope.removeuser = function (id,name,email) {
		$modal.open({
			templateUrl: 'pages/modal.html',
			controller: 'modalController',
			scope: $scope
		})
		.result.then(function() {
			$scope.confirmdelete(id,name,email)
		}, function() {

		});

	};
	$scope.errordelete= function (errMsg, tasks) {
		$scope.errorMessage = errMsg;
		$scope.pendingTasks = tasks;
		$modal.open({
			templateUrl: 'pages/errorDeletion.html',
			controller: 'modalController',
			scope: $scope
		})
		.result.then(function() {
		}
		);

	};

	$scope.message = 'Add Modify Delete Users.';
	$scope.userresult=false;
	$scope.error_in_deletion=false;

	/*
	 * $scope.roles = [ {'index':'R_1', 'name':'module Lead',
	 * 'type':'module-level'}, {'index':'R_2', 'name':'Project Manager',
	 * 'type':'project-level'}, {'index':'R_3', 'name':'Team Lead',
	 * 'type':'group-level'} ];
	 */

	$http.get("/whguserportal/services/roles/")
	.then(function (response) {$scope.roles = response.data;});

	$http.get("/whguserportal/services/modules/")
	.then(function (response) {$scope.modules = response.data;});
	$http.get("/whguserportal/services/groups/")
	.then(function (response) {$scope.groups = response.data;});
	$scope.showmodules=[];
	$scope.showgroupes=[];
	$scope.initshowothers=function(roleType,id){
		$scope.showmodules[id]=false;
		$scope.showgroupes[id]=false;

		if ((roleType)=="ModuleLevelAccess"){
			$scope.showmodules[id]=true;
			$scope.showgroupes[id]=true;
		};
		if ((roleType)=="GroupLevelAccess"){
			$scope.showgroupes[id]=true;

		};   
	};

	$scope.showothers=function(roleType,id,role,group){
		$scope.showmodules[id]=false;
		$scope.showgroupes[id]=false;
		if ((roleType)=="ProjectLevelAccess"){
			$scope.choices.splice(id,1,{'role':role});
		}; 
		if ((roleType)=="ModuleLevelAccess"){
			$scope.showmodules[id]=true;
			$scope.showgroupes[id]=true;
		};
		if ((roleType)=="GroupLevelAccess"){
			$scope.showgroupes[id]=true;
			$scope.choices.splice(id,1,{'role':role,'group':group});
		};   
	};
	$scope.showchanges=function(id){

		$scope.change=true;

	}
	$scope.$parent.editusers=false;
	$scope.$parent.reboardUser=false;
	$scope.$parent.republishTask=false;
	$scope.$parent.editTask=false;
	
	$scope.optg=[];
	$scope.optm=[];
	$scope.optr=[];
	$scope.groupoption=function(options,id){

		console.log(options)
		if(options!=null){
			$scope.optg[id]=options;
		}
		else{
			$scope.optg[id]="Choose Group ";


		}
		// $scope.opt="Choose Group"

	}


	$scope.roleoption=function(options,id){


		if(options!=null){
			$scope.optr[id]=options;

		}
		else{
			$scope.optr[id]="Choose Role ";


		}
	}
	/*
	 * $scope.roleoption=function(type,options,id){ console.log("gaya")
	 * console.log(options) if(type=="role"){ if(options!=null){
	 * $scope.optr[id]=options;} else{ $scope.optr[id]="Choose Role ";
	 * console.log("ye challa") }} else if(type=="group"){ if(options!=null){
	 * $scope.optg[id]=options; console.log("group chala") } else{
	 * $scope.optg[id]="Choose Group "; console.log(" else challa group ka") } }
	 * else if(type=="module"){ if(options!=null){ $scope.optm[id]=options;}
	 * else{ $scope.optm[id]="Choose Module"; console.log("ye challa") }
	 * 
	 *  } // $scope.opt="Choose Group" console.log( $scope.opt) }
	 */
	$scope.moduleoption=function(options,id){

		if(options!=null){
			$scope.optm[id]=options;}
		else{
			$scope.optm[id]="Choose Module";


		}
		// $scope.opt="Choose Group"

	}
	$scope.choices = [{}];

	$scope.addNewChoice = function() {
		var newItemNo = $scope.choices.length+1;
		$scope.choices.push({'id':newItemNo});
	};
	$scope.count=0;
	$scope.addModifyChoice = function(existinguser) {

		$scope.count=0;
		var userRoles = existinguser.userRoles;
		if(userRoles.length > 0){
			for(var i=0;i<userRoles.length;i++){
				$scope.choices.push({'id':userRoles[i].id,'role':userRoles[i].role,'group':userRoles[i].group,'module':userRoles[i].module});
				$scope.count=$scope.count+1;
			}
		}else{
			$scope.choices.push({});
		}
	};

	$scope.addupChoice = function() {
		var newItemNo = $scope.choices[$scope.choices.length-1].id+1;          
		$scope.choices.push({'id':newItemNo});

	};	 

	$scope.removeChoice = function(index) {
		var lastItem = $scope.choices.length-index;

		$scope.choices.splice(index,1);
	};
	$scope.count=0;
	$scope.formshow_users=function(){
		$scope.formshowEnabled_users = $scope.formshowEnabled_users== true ? false: true;
		$scope.formshowDeleted_users =false;
		// if( $scope.count==0){
		$http.get("/whguserportal/services/users/?excludeTasks=true&excludeUserRoles=true")
		.then(function (response) {$scope.users = response.data;});
		$scope.count=$scope.count+1;

		// }
	}


	$scope.count=0;
	$scope.formshowdeleted_users=function(){
		$scope.formshowDeleted_users = $scope.formshowDeleted_users== true ? false: true;
		$scope.formshowEnabled_users = false;
		// if( $scope.count==0){
		$http.get("/whguserportal/services/users/?excludeTasks=true&excludeUserRoles=true&deletedOnly=true")
		.then(function (response) {$scope.deletedusers = response.data;});
		$scope.count=$scope.count+1;

		// }
	}




	$scope.mailpresent=false;
	$scope.userModification=function(choices,edituser,editusers){
		$scope.userresult=true;
		$scope.newuserresult=true;
		console.log('isReBoarded? = '+edituser.deleted);
		edituser.reBoarded = edituser.deleted;
		if (editusers==true){
			$scope.newuserresult=false;
			$scope.urlgenerated="/whguserportal/services/users/add/?modify=true"
				console.log("edit")
				$scope.data={'id':edituser.id, 'name': edituser.name ,'email': edituser.email, 'access':edituser.access,'userRoles':choices,'adminId':$rootScope.globals.currentUser.id,'reBoarded':edituser.reBoarded}
		}else{
			$scope.urlgenerated="/whguserportal/services/users/add/"
				console.log("new")
				$scope.data={'name': edituser.name ,'email': edituser.email, 'access':edituser.access,'userRoles':choices,'adminId':$rootScope.globals.currentUser.id}
		}

		
		$http({
			url:  $scope.urlgenerated,
			dataType: 'json',
			method: 'PUT',
			data: $scope.data,
			headers: {
				"Content-Type": "application/json"
			}

		}).success(function(response){
			console.log("successfully user added ");
			this.response = response;
			$scope.addeduser=response

			return response;


		}).error(function(errResponse){
			console.log(errResponse)
			$scope.errResponse=errResponse
			$scope.mailpresent=true;


			console.error('Error while editing user');
		});

	};

	
	
	$scope.toggleEdit_user=function(index, isDeleted){
		$scope.go=index;
		$scope.formshowEnabled_users=!isDeleted;
		$scope.formshowDeleted_users=isDeleted;
		console.log("isDeleted? : " + isDeleted);
		$http.get("/whguserportal/services/users/"+index +"?excludeTasks=true")
		.then(function (response) {
			$scope.edituser = response.data;
			$scope.editusers=true;
			if(isDeleted){
				$scope.edituser.deleted = true;
				$scope.$parent.reboardUser = true;
			}else{
				$scope.$parent.reboardUser = false;
			}
			$scope.choices=[];
			$scope.addModifyChoice($scope.edituser);
		});
	}









	$scope.confirmdelete = function(id,name,email){
		$scope.urlgen="/whguserportal/services/users/delete/"
			$http({
				url:  $scope.urlgen,
				dataType: 'text',
				method: 'DELETE',
				data: {'id': id,'adminId':$rootScope.globals.currentUser.id,'name':name,'email':email},
				headers: {
					"Content-Type": "application/json"
				}
			}).success(function(response, status, headers, config){
				$scope.response = response;
				$scope.done=true;
				console.log(headers().message);
				if(headers().errormessage && headers().errorcode){
					console.log('error code: '+headers().errorcode+' || error message: '+headers().errormessage);
					var msg = ERROR_MESSAGES[headers().errorcode];
				
					
					$scope.errordelete(msg, response.tasks.Owner);
				}else{
					console.log("successfully deleted user ");
					$route.reload();
				}
			}).error(function(errResponse, status, headers, config){
				//$scope.error_in_deletion=true;
				console.log(errResponse);
				console.error('Error while deleting user');
				$scope.errordelete(errResponse.errorMessage);
			});
	}  

	$scope.userhome=function(){
		$scope.error_in_deletion=false;
		$scope.userresult=false;
		$route.reload();

	}
});





mainApp.controller('LoginController',

		function ($scope, $rootScope, $location, AuthenticationService, dataShare) {
	// reset login status
	AuthenticationService.ClearCredentials();

	$scope.login = function () {
		$scope.dataLoading = true;
		AuthenticationService.Login($scope.username, $scope.password, function(response,responsecr,errResponse) {

			/* if(response.email==$scope.username) { */
			if(responsecr.success) {


				AuthenticationService.SetCredentials(response);
				dataShare.dataObj.userid=response.id;

				$location.path('/');
			} else {

				$scope.error =responsecr.message;
				$scope.dataLoading = false;
			}
		});
	};
});
mainApp.controller('forgetpasswordController', function($scope, $http, dataShare,$rootScope,$route, $location) {

	$scope.forgetPassword=function(){

		$scope.urlgenerated="/whguserportal/services/users/login/forgot/"
			$scope.data={
				"email":$scope.email
		}

		console.log($scope.urlgenerated)
		console.log($scope.data)
		$http({
			url:  $scope.urlgenerated,
			dataType: 'json',
			method: 'POST',
			data: $scope.data,
			headers: {
				"Content-Type": "application/json"
			}

		}).success(function(response){
			console.log("service successfully called");
			$location.path('/login');
			return response;
		}).error(function(errResponse){
			$scope.error = true;
			$scope.forgotPasswordErrorMessage = 'Sorry we could not find you! Please enter a valid Email Address.';
			console.log(errResponse)
			console.error('Error while calling service');
		});

	}
})

mainApp.controller('passwordController', function($scope, $http, dataShare,$rootScope,$route,$modal,$location) {
	selectLeftNav();
	$scope.setnew=false;
	//$rootScope.changeemail=false;
	$scope.changePassword=true;
	$scope.errorshow=false;
	$scope.takehome=function(){
		console.log("hopefully")
		$location.path('/login');

	}

	$scope.clear = function(){
		$route.reload();
		console.log('cleared!');
	};

	//console.log($rootScope.globals)
	$scope.email=$rootScope.globals.currentUser.username;

	$scope.changeEmail=function(){
		$scope.changeemail=true;
	}
	$scope.passwordModification=function(new_password,confirmnew_password){
		console.log($rootScope.globals.currentUser.username)
		console.log($scope.email)
		console.log(new_password)
		console.log(confirmnew_password)
		$scope.userupdate=function(new_password){
			$scope.urlgenerated="/whguserportal/services/users/login/update/"
				$scope.data={
					"email":$scope.email,
					"id": $rootScope.globals.currentUser.id,
					"password": $scope.new_password,
					"name" : $rootScope.globals.currentUser.name
			}


			console.log($scope.urlgenerated)
			console.log($scope.data)
			$http({
				url:  $scope.urlgenerated,
				dataType: 'json',
				method: 'POST',
				data: $scope.data,
				headers: {
					"Content-Type": "application/json"
				}

			}).success(function(response){
				console.log("successfully password/emailid changed ");
				$scope.changePassword=false;
				return response;


			}).error(function(errResponse){
				console.log(errResponse)
				console.error('Error while editing user');
			});



		}

		if(new_password==confirmnew_password){
			$scope.userupdate(new_password);
		}
		else{
			$scope.errorshow=true;
			$scope.errordata="Password entered do not match each other"
		}
	}

})

mainApp.controller('systemController', function($scope, $http) {
	selectLeftNav();
	$http.get("/whguserportal/services/misc/getBackUpState").then(function (response) {
		if(response.data) {
			$scope.backupState = response.data.result;
		}
	});
});

mainApp.service('dataShare', function() {

	// private variable
	var _dataObj = {'id':'','change':false,'m1':'','userid':''};


	// public API
	this.dataObj = _dataObj;
	if(_dataObj.change==true)
		this.dataObj.mod= dataObj.task;



})

/*
 * mainApp.factory('dataShare',function($rootScope,$timeout){ var service = {};
 * service.data = false; service.sendData = function(data){ this.data = data;
 * $timeout(function(){ $rootScope.$broadcast('data_shared'); },500); };
 * service.getData = function(){ return this.data; }; return service; });
 */

